import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-privilege',
  templateUrl: './privilege.component.html',
  styleUrls: ['./privilege.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrivilegeComponent {

  constructor() { }

  static open(dialog: MatDialog): void {
    dialog.open(PrivilegeComponent);
  }

}
