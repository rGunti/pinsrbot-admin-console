import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { map, Observable } from 'rxjs';
import { FileHeader, FilesService } from 'src/app/features/file-explorer/files.service';

export interface FileSelectDialogArgs {
  channelId: string;
  mimeFilter?: string[];
}

@Component({
  templateUrl: './file-select-dialog.component.html',
  styleUrls: ['./file-select-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileSelectDialogComponent implements OnInit {

  files$: Observable<FileHeader[]>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly args: FileSelectDialogArgs,
    private readonly files: FilesService,
  ) {
    this.files$ = files.listFiles(args.channelId).pipe(
      map((l) => {
        if (args.mimeFilter) {
          return l.filter(f => args.mimeFilter!.some(i => f.mimeType.startsWith(i)));
        }
        return l;
      })
    );
  }

  static open(dialog: MatDialog, channelId: string, mimeFilter?: string[]): Observable<FileHeader | null | undefined> {
    const data: FileSelectDialogArgs = {
      channelId,
      mimeFilter
    };
    return dialog.open(FileSelectDialogComponent, {
      data,
    }).afterClosed();
  }

  ngOnInit(): void {
  }

  getIcon(file: FileHeader): string {
    return this.files.getFileIcon(file);
  }

}
