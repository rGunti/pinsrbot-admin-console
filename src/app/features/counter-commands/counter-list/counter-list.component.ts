import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, mergeMap, Observable, of, tap } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { P_EDIT_CUSTOM_COMMANDS } from 'src/app/permissions';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { CounterCommandStatus, CounterService } from '../counter.service';
import { RenameCounterCommandDialogComponent } from '../rename-counter-command-dialog/rename-counter-command-dialog.component';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './counter-list.component.html',
  styleUrls: ['./counter-list.component.scss']
})
export class CounterListComponent implements OnInit {

  channelFilter: string = EMPTY_FILTER;

  channels$: Observable<string[]> = of([]);
  commandDict: { [channelId: string]: Observable<CounterCommandStatus[] | null> } = {};
  commands$: Observable<CounterCommandStatus[] | null> = of(null);

  hasEditPermission: boolean = false;
  isLoading: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly users: UserService,
    private readonly pageTitle: PageTitleService,
    private readonly commands: CounterService,
    private readonly dialog: GenericDialogService,
    private readonly matDialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Counter Commands', 'svg:counter');
    this.initFromRoute();
    this.loadChannels();
    this.users.hasPermission(P_EDIT_CUSTOM_COMMANDS).subscribe({
      next: (hasPerm) => this.hasEditPermission = hasPerm
    });
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel,
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
      this.loadCommandsFromChannel(this.channelFilter);
    }
  }

  loadChannels(): void {
    this.channels$ = this.users.me().pipe(
      map((u) => u.ownerOf)
    );
  }

  private loadCommandsFromChannel(channelId: string): void {
    this.commandDict[channelId] = this.commands.getCommandsForChannel(channelId).pipe(
      tap(() => {
        this.isLoading = false;
      })
    );
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onPanelOpened(channel: string): void {
    this.channelFilter = channel;
    this.updateRoute();
    this.loadCommandsFromChannel(channel);
  }

  onPanelClosed(channel: string): void {
    this.channelFilter = EMPTY_FILTER;
    this.updateRoute();
    setTimeout(() => {
      this.commandDict[channel] = of();
    }, 1000);
  }

  createCommand(): void {
    this.router.navigate(['_'], {
      relativeTo: this.activatedRoute,
    });
  }

  getEditLink(command: CounterCommandStatus): string[] {
    return ['../../', ...command.channelId.split('/'), command.name];
  }

  async deleteCommand(command: CounterCommandStatus): Promise<void> {
    if (!(await this.dialog.ask(
      `Delete "${command.name}"`,
      `Do you want to delete the command "${command.name}" from this channel?`
    ))) {
      return;
    }
    this.commands.deleteCommand(command.channelId, command.name).subscribe({
      complete: () => {
        this.dialog.snack('Command deleted successfully');
        this.loadCommandsFromChannel(command.channelId);
      },
    });
  }

  async onResetCounter(command: CounterCommandStatus): Promise<void> {
    if (!(await this.dialog.ask(
      `Reset "${command.name}"`,
      `Do you want to reset the counter back to 0?`
    ))) {
      return;
    }
    this.commands.resetCommand(command.channelId, command.name).subscribe({
      complete: () => {
        this.dialog.snack('Command has been reset');
        this.loadCommandsFromChannel(command.channelId);
      }
    });
  }

  renameCommand(command: CounterCommandStatus): void {
    RenameCounterCommandDialogComponent.open(
      this.matDialog,
      command
    ).pipe(
      filter((s) => !!s),
      mergeMap((newCommandName) => this.commands.renameCommand(
        command.channelId,
        command.name,
        newCommandName!,
      ))
    ).subscribe({
      next: () => {
        this.loadCommandsFromChannel(this.channelFilter);
      }
    });
  }

}
