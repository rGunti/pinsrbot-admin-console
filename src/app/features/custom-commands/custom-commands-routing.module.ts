import { P_EDIT_CUSTOM_COMMANDS, P_READ_CUSTOM_COMMANDS } from './../../permissions';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { CommandListComponent } from './command-list/command-list.component';
import { CommandEditorComponent } from './command-editor/command-editor.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '_/_',
  },
  {
    path: ':messageInterface/:channel',
    component: CommandListComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_READ_CUSTOM_COMMANDS,
    }
  },
  {
    path: ':messageInterface/:channel/:command',
    component: CommandEditorComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_EDIT_CUSTOM_COMMANDS,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomCommandsRoutingModule { }
