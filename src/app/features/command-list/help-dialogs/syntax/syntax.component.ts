import { Component, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-syntax',
  templateUrl: './syntax.component.html',
  styleUrls: ['./syntax.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SyntaxComponent {

  constructor() { }

  static open(dialog: MatDialog): void {
    dialog.open(SyntaxComponent);
  }

}
