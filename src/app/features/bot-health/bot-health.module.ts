import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { MomentModule } from 'ngx-moment';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { BotHealthRoutingModule } from './bot-health-routing.module';
import { BotHealthComponent } from './bot-health.component';


@NgModule({
  declarations: [
    BotHealthComponent
  ],
  imports: [
    CommonModule,
    BotHealthRoutingModule,

    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatProgressBarModule,

    MomentModule,
    CommonControlsModule,
  ]
})
export class BotHealthModule { }
