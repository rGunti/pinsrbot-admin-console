import { MatPaginator } from '@angular/material/paginator';
import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, of, map } from 'rxjs';
import { CommandConfigService, CommandConfiguration, CommandConfigurationDto } from '../command-config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

@Component({
  selector: 'app-command-config-tab',
  templateUrl: './command-config-tab.component.html',
  styleUrls: ['./command-config-tab.component.scss']
})
export class CommandConfigTabComponent implements OnInit {

  columns = [
    'enabled',
    'name',
    'aliases',
    'description',
    'restrictedToInterfaces',
    'privileges',
    'functions'
  ];

  private _channelId!: string;

  commands$: Observable<MatTableDataSource<CommandConfigurationDto> | null> = of(null);

  @Input() paginator!: MatPaginator;
  @Input() hasEditPermission: boolean = false;

  constructor(
    private readonly configService: CommandConfigService,
    private readonly dialog: GenericDialogService,
  ) { }

  @Input() get channelId(): string {
    return this._channelId;
  }
  set channelId(channelId: string) {
    this._channelId = channelId;
    this.updateConfigs();
  }

  ngOnInit(): void {
    this.updateConfigs();
  }

  private updateConfigs(): void {
    if (!this.channelId) {
      return;
    }
    this.commands$ = this.configService.getConfigs(this.channelId).pipe(
      map(c => {
        const source = new MatTableDataSource<CommandConfigurationDto>(c);
        source.paginator = this.paginator;
        return source;
      })
    );
  }

  getEditRoute(config: CommandConfigurationDto): string[] {
    return ['../../', ...this.channelId.split('/'), `${config.info.name}`];
  }

  async deleteConfig(config: CommandConfigurationDto): Promise<void> {
    if (!await this.dialog.ask(
      `Reset Configuration for Command ${config.info.name}`,
      'Would you like to reset the configuration for this command?'
    )) {
      return;
    }

    this.configService.deleteConfig(this.channelId, config.info.name).subscribe({
      next: () => {
        this.dialog.snack('Command configuration reset to defaults');
        this.updateConfigs();
      },
      error: () => {
        this.dialog.snack('Failed to reset configuration');
      }
    });
  }

}
