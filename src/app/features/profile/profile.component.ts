import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { map } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user$ = this.auth.user$;
  code$ = this.user$.pipe(map((user) => JSON.stringify(user, null, 2)));
  pinsUser$ = this.userService.me();
  permissions$ = this.userService.myPermissions();

  constructor(
    private readonly auth: AuthService,
    private readonly userService: UserService,
    private readonly title: PageTitleService,
  ) { }

  ngOnInit(): void {
    this.title.setPageTitle('Your Profile', 'account_circle');
  }

}
