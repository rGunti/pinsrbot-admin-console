import { MatDialogModule } from '@angular/material/dialog';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { CustomCommandsRoutingModule } from './custom-commands-routing.module';
import { CommandListComponent } from './command-list/command-list.component';
import { CommandEditorComponent } from './command-editor/command-editor.component';
import { RenameCustomCommandDialogComponent } from './rename-custom-command-dialog/rename-custom-command-dialog.component';


@NgModule({
  declarations: [
    CommandListComponent,
    CommandEditorComponent,
    RenameCustomCommandDialogComponent
  ],
  imports: [
    CommonModule,
    CustomCommandsRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatExpansionModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatListModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatDialogModule,

    CommonControlsModule,
  ]
})
export class CustomCommandsModule { }
