import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GenericDialogSettings } from './generic-dialog.entities';

@Component({
  selector: 'app-generic-dialog',
  templateUrl: './generic-dialog.component.html',
  styleUrls: ['./generic-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) private readonly data: GenericDialogSettings,
  ) { }

  get title(): string {
    return this.data.title;
  }

  get hasMessage(): boolean {
    return !!this.data.message;
  }

  get message(): string {
    return this.data.message!;
  }

  get icon(): string {
    return this.data.icon!;
  }

  get color(): ThemePalette {
    return this.data.color;
  }

  get hasYesNoButtons(): boolean {
    return this.data.type === 'yes-no';
  }

  get hasOkButton(): boolean {
    return this.data.type === 'ok';
  }

  get hasIcon(): boolean {
    return !!this.data.icon;
  }

  get hasSvgIcon(): boolean {
    return !!this.data.useSvgIcon;
  }

}
