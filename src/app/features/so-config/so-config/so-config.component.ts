import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { ShoutoutMessageSettings, SoConfigService } from '../so-config.service';

@Component({
  templateUrl: './so-config.component.html',
  styleUrls: ['./so-config.component.scss']
})
export class SoConfigComponent implements OnInit {

  config$: Observable<ShoutoutMessageSettings[] | null> = of(null);
  expandedChannelId: string | undefined;

  constructor(
    private readonly soConfig: SoConfigService,
    private readonly pageTitle: PageTitleService,
    private readonly snackbar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Shoutout', 'campaign');
    this.load();
  }

  trackConfig(index: number, config: ShoutoutMessageSettings): string {
    return config.id;
  }

  load(): void {
    this.config$ = this.soConfig.getMessage();
  }

  clearMessage(config: ShoutoutMessageSettings): void {
    config.message = '';
  }

  saveConfig(config: ShoutoutMessageSettings): void {
    this.soConfig.updateMessage(config).subscribe({
      next: () => {
        this.snackbar.open('Shoutout Message saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

  saveAllChanges(configs: ShoutoutMessageSettings[]): void {
    this.soConfig.updateMessages(configs).subscribe({
      next: () => {
        this.snackbar.open('Shoutout Messages saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

}
