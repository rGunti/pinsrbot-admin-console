import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { FileExplorerRoutingModule } from './file-explorer-routing.module';
import { HomeComponent } from './home/home.component';
import { FileListComponent } from './file-list/file-list.component';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';


@NgModule({
  declarations: [
    HomeComponent,
    FileListComponent,
    UploadDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatDialogModule,

    FileExplorerRoutingModule,
    CommonControlsModule,
  ]
})
export class FileExplorerModule { }
