import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, tap } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { Quote, QuotesService } from '../quotes.service';

@Component({
  templateUrl: './quote-editor.component.html',
  styleUrls: ['./quote-editor.component.scss']
})
export class QuoteEditorComponent implements OnInit {

  quote$: Observable<Quote | null> = of(null);

  form: UntypedFormGroup;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly quote: QuotesService,
    fb: UntypedFormBuilder,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) {
    this.form = fb.group({
      quoteText: ['', [
        Validators.required,
        Validators.maxLength(400)
      ]],
      quoteContext: ['', [
        Validators.required,
        Validators.maxLength(200)
      ]],
    });
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Quotes', 'format_quote');
    const {
      messageInterface,
      channel,
      quoteNumber
    } = this.activatedRoute.snapshot.params;
    this.quote$ = this.quote.getQuote(messageInterface, channel, quoteNumber).pipe(
      tap((q) => this.updateForm(q))
    );
  }

  private updateForm(quote: Quote): void {
    this.pageTitle.setPageTitle(`Quote #${quote.quoteId}`, 'format_quote');
    this.form.patchValue(quote);
  }

  saveQuote(refQuote: Quote): void {
    const newQuote = {
      ...refQuote,
      ...this.form.value
    };
    this.quote.updateQuote(newQuote).subscribe({
      next: () => {
        this.dialog.snack('Quote updated');
        this.router.navigate(['../'], {
          relativeTo: this.activatedRoute
        });
      },
      error: (e) => {
        alert(`Could not update quote due to an error: ${e.message}`);
      }
    });
  }

}
