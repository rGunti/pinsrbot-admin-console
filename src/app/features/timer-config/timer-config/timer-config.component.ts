import { Observable, of, map } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { TimerConfigService, TimerMessage, TimerMessageConfig } from '../timer-config.service';
import { PageTitleService } from 'src/app/service/page-title.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  templateUrl: './timer-config.component.html',
  styleUrls: ['./timer-config.component.scss']
})
export class TimerConfigComponent implements OnInit {

  configs$: Observable<TimerMessageConfig[] | null> = of(null);
  expandedChannelId?: string;

  constructor(
    private readonly timerConfig: TimerConfigService,
    private readonly pageTitle: PageTitleService,
    private readonly snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Timer Messages', 'av_timer');
    this.load();
  }

  load(): void {
    this.configs$ = this.timerConfig.getConfigs().pipe(
      map(a => {
        for (const c of a) {
          if (!c.messages || c.messages.length === 0) {
            c.messages.push({ message: '' });
          }
        }
        return a;
      })
    );
  }

  saveAllChanges(configs: TimerMessageConfig[]): void {
    configs.forEach(c => {
      c.messages = c.messages.filter(m => !!m.message);
    });
    this.timerConfig.updateConfigs(configs).subscribe({
      next: () => {
        this.snackbar.open('Timer Messages saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

  trackConfig(index: number, config: TimerMessageConfig): string {
    return config.id;
  }

  trackMessage(index: number, message: TimerMessage): string {
    return `${index}`;
  }

  addMessage(config: TimerMessageConfig): void {
    config.messages.push({ message: '' });
  }

  deleteMessage(config: TimerMessageConfig, messageIndex: number): void {
    config.messages = [
      ...config.messages.slice(0, messageIndex),
      ...config.messages.slice(messageIndex + 1)
    ];
  }

  saveConfig(config: TimerMessageConfig): void {
    config.messages = config.messages.filter(m => !!m.message);
    this.timerConfig.updateConfig(config).subscribe({
      next: () => {
        this.snackbar.open('Timer Messages saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

}
