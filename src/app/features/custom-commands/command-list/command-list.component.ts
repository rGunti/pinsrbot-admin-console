import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, mergeMap, Observable, of, switchMap, tap } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { P_EDIT_CUSTOM_COMMANDS } from 'src/app/permissions';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { CustomCommand, CustomCommandsService } from '../custom-commands.service';
import { RenameCustomCommandDialogComponent } from '../rename-custom-command-dialog/rename-custom-command-dialog.component';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './command-list.component.html',
  styleUrls: ['./command-list.component.scss']
})
export class CommandListComponent implements OnInit {

  channelFilter: string = EMPTY_FILTER;

  channels$: Observable<string[]> = of([]);
  commandDict: { [channelId: string]: Observable<CustomCommand[] | null> } = {};
  commands$: Observable<CustomCommand[] | null> = of(null);

  hasEditPermission: boolean = false;
  isLoading: boolean = false;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly users: UserService,
    private readonly commands: CustomCommandsService,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
    private readonly matDialog: MatDialog,
  ) { }

  get emptyFilter(): string { return EMPTY_FILTER; }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Custom Commands', 'border_color');
    this.initFromRoute();
    this.loadChannels();
    this.users.hasPermission(P_EDIT_CUSTOM_COMMANDS).subscribe({
      next: (hasPerm) => this.hasEditPermission = hasPerm
    });
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel,
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
      this.loadCommandsFromChannel(this.channelFilter);
    }
  }

  loadChannels(): void {
    this.channels$ = this.users.me().pipe(
      map((u) => u.ownerOf)
    );
  }

  private loadCommandsFromChannel(channelId: string): void {
    this.commandDict[channelId] = this.commands.getCommandsForChannel(channelId).pipe(
      tap(() => {
        this.isLoading = false;
      })
    );
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onPanelOpened(channel: string): void {
    this.channelFilter = channel;
    this.updateRoute();
    this.loadCommandsFromChannel(channel);
  }

  onPanelClosed(channel: string): void {
    this.channelFilter = EMPTY_FILTER;
    this.updateRoute();
    setTimeout(() => {
      this.commandDict[channel] = of();
    }, 1000);
  }

  createCommand(): void {
    this.router.navigate(['_'], {
      relativeTo: this.activatedRoute,
    });
  }

  getEditLink(command: CustomCommand): string[] {
    return ['../../', ...command.channel.split('/'), command.command];
  }

  async deleteCommand(command: CustomCommand): Promise<void> {
    if (!(await this.dialog.ask(
      `Delete "${command.command}"`,
      `Do you want to delete the command "${command.command}" from this channel?`
    ))) {
      return;
    }
    this.commands.deleteCommand(command.channel, command.command).subscribe({
      complete: () => {
        this.dialog.snack('Command deleted successfully');
        this.loadCommandsFromChannel(command.channel);
      },
    });
  }

  async renameCommand(command: CustomCommand): Promise<void> {
    RenameCustomCommandDialogComponent.open(
      this.matDialog,
      command
    ).pipe(
      filter((s) => !!s),
      mergeMap((newCommandName) => this.commands.renameCommand(
        command.channel,
        command.command,
        newCommandName!,
      )),
    ).subscribe({
      next: () => {
        this.loadCommandsFromChannel(this.channelFilter);
      }
    })
  }

}
