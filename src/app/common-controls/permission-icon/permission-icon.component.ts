import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-permission-icon',
  templateUrl: './permission-icon.component.html',
  styleUrls: ['./permission-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PermissionIconComponent {

  @Input() privilegeLevel!: string;

  constructor() { }

  get privilegeIcon(): string {
    switch (this.privilegeLevel) {
      case 'Administrator':
        return 'shield-crown';
      case 'Moderator':
        return 'sword';
      case 'Viewer':
        return 'account';
      default:
        return 'account-question';
    }
  }

}
