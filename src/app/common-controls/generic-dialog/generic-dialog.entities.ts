import { ThemePalette } from "@angular/material/core";

export interface GenericDialogSettings {
  title: string;
  message?: string;
  icon?: string;
  useSvgIcon?: boolean;
  type: 'yes-no' | 'ok';
  color: ThemePalette;
}

export type GenericDialogResponse = boolean;
