import { PageTitleService } from './../../../service/page-title.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, UntypedFormArray, UntypedFormBuilder, FormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, filter, mergeMap, Observable, of, Subscription, tap, catchError, map, first } from 'rxjs';
import { CustomCommand, CustomCommandsService } from '../custom-commands.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

const NEW_COMMAND_NAME = '_';

@Component({
  templateUrl: './command-editor.component.html',
  styleUrls: ['./command-editor.component.scss']
})
export class CommandEditorComponent implements OnInit, OnDestroy {

  command$: Observable<CustomCommand | null> = of(null);
  commandName: string = NEW_COMMAND_NAME;

  form: UntypedFormGroup;

  commandNameDuplicate$: Observable<boolean>;

  commandCounterValue$?: Observable<number>;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly command: CustomCommandsService,
    private readonly fb: UntypedFormBuilder,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) {
    this.form = fb.group({
      channel: ['', [Validators.required]],
      command: ['', [
        Validators.required,
        Validators.pattern('[a-z]{1,}'),
      ]],
      response: ['', [
        Validators.required,
        Validators.maxLength(450),
      ]],
      responseVariants: this.fb.array([]),
      limitedToMod: [false],
      limitedToUsers: this.fb.array([]),
      timeout: [0, [Validators.required, Validators.pattern(/\d{1,6}/)]],
    });

    this.commandNameDuplicate$ = this.form.get('command')!.valueChanges.pipe(
      debounceTime(500),
      filter((commandName: string) => !!commandName && (this.inCreationMode || commandName === this.commandName)),
      map((commandName: string) => commandName.toLowerCase()),
      mergeMap((commandName) => {
        const channel = this.form.get('channel')!.value;
        return this.command.getCommand(channel, commandName).pipe(
          catchError((e) => of(null)),
        );
      }),
      map((c) => !!c),
      tap((exists) => {
        if (exists) {
          this.form.get('command')!.setErrors({
            commandExists: true
          });
        }
      })
    );
  }

  get inCreationMode(): boolean {
    return this.commandName === NEW_COMMAND_NAME;
  }

  get limitedToModsChecked(): boolean {
    return this.form.get('limitedToMod')?.value || false;
  }

  get whitelistLength(): number {
    return this.limitUserControls.length;
  }

  get limitUserControls(): UntypedFormArray {
    return this.form.get('limitedToUsers') as UntypedFormArray;
  }

  get responseVariants(): UntypedFormArray {
    return this.form.get('responseVariants') as UntypedFormArray;
  }

  get responseLength(): number {
    return (this.form.get('response')?.value as string | undefined)?.length || 0;
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Custom Commands', 'border_color');

    const {
      messageInterface,
      channel,
      command
    } = this.activatedRoute.snapshot.params;

    this.commandName = command;
    let command$: Observable<CustomCommand>;
    if (command === NEW_COMMAND_NAME) {
      const newCommand: CustomCommand = {
        channel: `${messageInterface}/${channel}`,
        command: '',
        response: '',
        limitedToMod: false,
        limitedToUsers: [],
        timeout: 0,
      };
      command$ = of(newCommand);
      this.commandCounterValue$ = undefined;
      this.pageTitle.setPageTitle(`New Custom Command`, 'border_color');
    } else {
      command$ = this.command.getCommand(`${messageInterface}/${channel}`, command).pipe(
        tap((c) => {
          this.pageTitle.setPageTitle(`Custom Command "${c.command}"`, 'border_color');
        })
      );
      this.updateCounterValue(`${messageInterface}/${channel}`, command);
    }
    this.command$ = command$.pipe(
      tap((c) => this.updateForm(c))
    );
  }

  private updateCounterValue(channelId: string, commandName: string): void {
    this.commandCounterValue$ = this.command.getCounterValue(
      channelId, commandName);
  }

  ngOnDestroy(): void {
  }

  private updateForm(command: CustomCommand): void {
    this.form.patchValue(command);
    command.limitedToUsers.forEach(u => this.addUser(u));
    command.responseVariants?.forEach(v => this.addVariant(v))
  }

  addUser(value: string = ''): void {
    const userForm = this.fb.control(value, [Validators.required]);
    this.limitUserControls.push(userForm);
  }

  addVariant(value: string = ''): void {
    const variantForm = this.fb.control(value, [
      Validators.required,
      Validators.maxLength(400),
    ]);
    this.responseVariants.push(variantForm);
    this.updateFormState();
  }

  removeUser(i: number): void {
    this.limitUserControls.removeAt(i);
  }

  removeVariant(i: number): void {
    this.responseVariants.removeAt(i);
    this.updateFormState();
  }

  private updateFormState() {
    const response = this.form.get('response');
    if (this.responseVariants.length > 0) {
      response?.disable();
    } else {
      response?.enable();
    }
  }

  saveCommand(command: CustomCommand): void {
    if (!this.form.valid) {
      console.log('Form Errors', this.form);
      alert('Cannot save. Please check your entries and try again.');
      return;
    }
    const newCommand: CustomCommand = {
      ...command,
      ...this.form.value,
    };

    // Force command name to be lower case
    newCommand.command = newCommand.command.toLowerCase();
    // Force user names to be lower case
    newCommand.limitedToUsers = newCommand.limitedToUsers
      .filter(u => !!u)
      .map(u => u.toLowerCase());

    if (this.inCreationMode) {
      // Create Command
      this.command.createCommand(newCommand).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not create command due to an error: ${e.message}`);
        }
      });
    } else {
      // Update Command
      this.command.updateCommand(newCommand).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not update command due to an error: ${e.message}`);
        }
      });
    }
  }

  updateCounter(newValue: number): void {
    const command = this.form.value as CustomCommand;
    this.command.updateCounterValue(`${command.channel}`, command.command, newValue).subscribe({
      next: () => {
        this.updateCounterValue(command.channel, command.command);
        this.dialog.snack('Counter updated');
      },
      error: () => {
        this.dialog.snack('Failed to update Counter');
      }
    });
  }

  async clearCounter(): Promise<void> {
    if (!(await this.dialog.ask(
      'Clear Counter Value',
      'Do you want to clear (a.k.a. reset) the Counter value for this command?'
    ))) {
      return;
    }

    const command = this.form.value as CustomCommand;

    this.command.clearCounterValue(command.channel, command.command).subscribe({
      next: () => {
        this.updateCounterValue(command.channel, command.command);
        this.dialog.snack('Counter reset');
      },
      error: () => {
        this.dialog.snack('Failed to reset Counter');
      }
    });
  }

}
