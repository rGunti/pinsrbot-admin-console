import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

import { MarkdownModule } from 'ngx-markdown';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { PatchNotesRoutingModule } from './patch-notes-routing.module';
import { PatchNotesComponent } from './patch-notes.component';


@NgModule({
  declarations: [
    PatchNotesComponent
  ],
  imports: [
    CommonModule,
    PatchNotesRoutingModule,

    MatIconModule,
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,

    MarkdownModule.forChild(),

    CommonControlsModule,
  ]
})
export class PatchNotesModule { }
