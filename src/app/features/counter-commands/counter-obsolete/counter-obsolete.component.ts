import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-counter-obsolete',
  templateUrl: './counter-obsolete.component.html',
  styleUrls: ['./counter-obsolete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterObsoleteComponent implements OnInit {

  inV2Mode: boolean = environment.v2Mode;

  constructor() { }

  ngOnInit(): void {
  }

}
