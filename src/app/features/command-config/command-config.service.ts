import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';
import { environment } from 'src/environments/environment';
import { Command } from '../command-list/command-list.service';

export interface CommandConfiguration {
  id?: string;
  channelId: string;
  commandName: string;
  privilegeLevel?: string;
  disabled: boolean;
  cooldown: boolean;
  cooldownConfig: {
    cooldown: number;
    modCooldown: number;
    adminCooldown: number;
  };
}

export interface CommandConfigurationDto {
  info: Command;
  config?: CommandConfiguration;
}

@Injectable({
  providedIn: 'root'
})
export class CommandConfigService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, `/api/v1/commands/config`);
  }

  getConfigs(channelId: string): Observable<CommandConfigurationDto[]> {
    return this.http.get<CommandConfigurationDto[]>(this.getUrl(`/${channelId}`));
  }

  getConfig(messageInterface: string, channel: string, command: string): Observable<CommandConfigurationDto> {
    return this.http.get<CommandConfigurationDto>(this.getUrl(`/${messageInterface}/${channel}/${command}`));
  }

  updateConfig(channelId: string, command: CommandConfiguration): Observable<void> {
    return this.http.put<void>(this.getUrl(`/${channelId}/${command.commandName}`), command);
  }

  deleteConfig(channelId: string, command: string): Observable<void> {
    return this.http.delete<void>(this.getUrl(`/${channelId}/${command}`));
  }
}
