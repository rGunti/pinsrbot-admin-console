#!/usr/bin/env sh
P_IMAGE_SUFFIX=${1}
P_ARTIFACT_FILE_BASE=${2}

IMAGE_NAME="${CI_REGISTRY_IMAGE}${P_IMAGE_SUFFIX}"

# shellcheck disable=SC2112
function d_load() {
  p_file=$1
  echo "Loading image file ${p_file} ..."
  docker load -i "${p_file}"
}

# shellcheck disable=SC2112
function d_push() {
  p_image=$1
  echo "Pushing image ${p_image}"
  docker push "${p_image}"
}

set -e

# - Loading
d_load "${P_ARTIFACT_FILE_BASE}.tar"

# - Pushing
d_push "${IMAGE_NAME}:commit-${CI_COMMIT_SHORT_SHA}"
if [ "$CI_COMMIT_BRANCH" = "master" ]; then
  d_push "${IMAGE_NAME}:master"
fi
if [ "$CI_COMMIT_TAG" ]; then
  # Parse Version out
  . scripts/semver.sh
  V_MAJOR=0
  V_MINOR=0
  V_PATCH=0
  V_SPECIAL=""
  semverParseInto "$CI_COMMIT_TAG" V_MAJOR V_MINOR V_PATCH V_SPECIAL

  d_push "${IMAGE_NAME}:latest"
  d_push "${IMAGE_NAME}:${CI_COMMIT_TAG}"
  d_push "${IMAGE_NAME}:${V_MAJOR}.${V_MINOR}"
fi
