import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  inV2Mode = environment.v2Mode;

  constructor(
    private readonly userService: AuthService,
    private readonly pageTitle: PageTitleService,
  ) { }

  get isLoggedIn$(): Observable<boolean> {
    return this.userService.isAuthenticated$;
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Home', 'home');
  }

}
