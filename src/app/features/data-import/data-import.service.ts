import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';
import { StatusReport, uploadProgress } from 'src/app/utils/operators';

@Injectable({
  providedIn: 'root'
})
export class DataImportService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, `/api/v1/import`);
  }

  uploadFile(file: File, simulate: boolean): Observable<StatusReport> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<void>(this.getUrl(`?simulate=${simulate}`), formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      uploadProgress()
    );
  }
}
