import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { SoundCommand } from './../sound-commands.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, mergeMap, Observable, of, tap } from 'rxjs';
import { P_EDIT_CUSTOM_COMMANDS } from 'src/app/permissions';
import { UserService } from 'src/app/user.service';
import { SoundCommandsService } from '../sound-commands.service';
import { PageTitleService } from 'src/app/service/page-title.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { MatDialog } from '@angular/material/dialog';
import { RenameSoundCommandDialogComponent } from '../rename-sound-command-dialog/rename-sound-command-dialog.component';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './sound-command-list.component.html',
  styleUrls: ['./sound-command-list.component.scss']
})
export class SoundCommandListComponent implements OnInit {

  channelFilter: string = EMPTY_FILTER;

  channels$: Observable<string[]> = of([]);
  commands$: Observable<SoundCommand[] | null> = of(null);
  commandDict: { [channelId: string]: Observable<SoundCommand[] | null> } = {};

  hasEditPermission: boolean = false;
  isLoading: boolean = false;

  previewAudioSource: SafeUrl | undefined;
  previewVolume = 20;
  previewMuted = false;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly users: UserService,
    private readonly commands: SoundCommandsService,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
    private readonly sanitizer: DomSanitizer,
    private readonly matDialog: MatDialog,
  ) { }

  get emptyFilter(): string { return EMPTY_FILTER; }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Sound Commands', 'volume_up');
    this.initFromRoute();
    this.loadChannels();
    this.users.hasPermission(P_EDIT_CUSTOM_COMMANDS).subscribe({
      next: (hasPerm) => this.hasEditPermission = hasPerm
    });
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel,
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
      this.loadCommandsFromChannel(this.channelFilter);
    }
  }

  loadChannels(): void {
    this.channels$ = this.users.me().pipe(
      map((u) => u.ownerOf)
    );
  }

  private loadCommandsFromChannel(channelId: string): void {
    this.commandDict[channelId] = this.commands.getCommandsForChannel(channelId).pipe(
      tap(() => {
        this.isLoading = false;
      })
    );
  }

  loadCommands(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.commands$ = this.commands.getCommandsForChannel(this.channelFilter);
    } else {
      this.commands$ = this.commands.getCommands();
    }
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onFilterChange(): void {
    this.updateRoute();
    this.loadCommands();
  }

  createCommand(): void {
    this.router.navigate(['_'], {
      relativeTo: this.activatedRoute,
    });
  }

  getEditLink(command: SoundCommand): string[] {
    return ['../../', ...command.channelId.split('/'), command.commandName];
  }

  getUploadLink(command: SoundCommand): string[] {
    return ['../../', ...command.channelId.split('/'), command.commandName, 'upload'];
  }

  getSoundBoardLink(): string[] {
    return ['../../', ...this.channelFilter.split('/'), '_', 'board'];
  }

  deleteCommand(command: SoundCommand): void {
    if (!confirm(`Do you want to delete the command ${command.commandName} from ${command.channelId}?`)) {
      return;
    }
    this.commands.deleteCommand(command.channelId, command.commandName).subscribe({
      complete: () => this.loadCommands(),
    });
  }

  onPanelOpened(channel: string): void {
    this.channelFilter = channel;
    this.updateRoute();
    this.loadCommandsFromChannel(channel);
  }

  onPanelClosed(channel: string): void {
    this.channelFilter = EMPTY_FILTER;
    this.updateRoute();
    setTimeout(() => {
      this.commandDict[channel] = of();
    }, 1000);
  }

  formatVolumeLabel(value: number) {
    return `${value}%`;
  }

  playSoundPreview(command: SoundCommand): void {
    this.commands.getSoundFile(command.channelId, command.commandName).subscribe({
      next: (b) => {
        this.previewAudioSource = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(b));
        this.dialog.snack(`Playing "${command.commandName}"`);
      },
      error: () => {
        this.dialog.snack('Failed to play back preview');
      }
    })
  }

  downloadLegacySoundFile(command: SoundCommand): void {
    this.commands.getSoundFile(command.channelId, command.commandName).subscribe({
      next: (b) => {
        let url = window.URL.createObjectURL(b);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = `${command.commandName}.wav`;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      }
    });
  }

  renameCommand(command: SoundCommand): void {
    RenameSoundCommandDialogComponent.open(
      this.matDialog,
      command
    ).pipe(
      filter((s) => !!s),
      mergeMap((newCommandName) => this.commands.renameCommand(
        command.channelId,
        command.commandName,
        newCommandName!,
      ))
    ).subscribe({
      next: () => {
        this.loadCommandsFromChannel(this.channelFilter);
      }
    });
  }

}
