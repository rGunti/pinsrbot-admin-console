import { Component, ChangeDetectionStrategy, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-channel-name',
  templateUrl: './channel-name.component.html',
  styleUrls: ['./channel-name.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChannelNameComponent {

  @Input() channelId?: string;
  @Input() resolveAlias: boolean = true;

  constructor() { }

}
