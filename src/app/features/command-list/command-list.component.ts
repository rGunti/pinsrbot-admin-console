import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { Command, CommandListService } from './command-list.service';
import { PrivilegeComponent } from './help-dialogs/privilege/privilege.component';
import { SyntaxComponent } from './help-dialogs/syntax/syntax.component';

@Component({
  selector: 'app-command-list',
  templateUrl: './command-list.component.html',
  styleUrls: ['./command-list.component.scss']
})
export class CommandListComponent implements OnInit {

  commands$: Observable<Command[] | null> = of(null);

  constructor(
    private readonly commandList: CommandListService,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Commands', 'priority_high');
    this.commands$ = this.commandList.getCommands();
  }

  getList(list: string[], separator: string = ', '): string {
    return (list || []).join(separator);
  }

  limitedToInterface(command: Command, msgIf: string): boolean {
    return command.restrictedToInterfaces && command.restrictedToInterfaces.indexOf(msgIf) >= 0;
  }

  getPrivilegeLevelIcon(level: string): string {
    switch (level) {
      case 'Unknown':
        return 'account-question';
      case 'Viewer':
        return 'account';
      case 'Moderator':
        return 'sword';
      case 'Administrator':
        return 'shield-crown';
      default:
        throw new Error(`Level Icon not defined for ${level}`);
    }
  }

  scrollTo(el: HTMLElement): void {
    el.scrollIntoView();
  }

  openSyntaxDialog(): void {
    SyntaxComponent.open(this.dialog);
  }

  openPrivilegeDialog(): void {
    PrivilegeComponent.open(this.dialog);
  }

}
