import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SoConfigComponent } from './so-config/so-config.component';

const routes: Routes = [{ path: '', component: SoConfigComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoConfigRoutingModule { }
