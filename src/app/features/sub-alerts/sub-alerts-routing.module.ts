import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubAlertsComponent } from './sub-alerts.component';

const routes: Routes = [{ path: '', component: SubAlertsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAlertsRoutingModule { }
