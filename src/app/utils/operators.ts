import { HttpEvent, HttpEventType } from '@angular/common/http';
import { map, OperatorFunction } from 'rxjs';

export interface StatusReport {
  percentage: number;
}

export function uploadProgress(): OperatorFunction<HttpEvent<void>, StatusReport> {
  return map((e: HttpEvent<void>) => {
    switch (e.type) {
      case HttpEventType.DownloadProgress:
      case HttpEventType.UploadProgress:
        return {
          percentage: Math.round((100 * e.loaded) / e.total!),
        };
      case HttpEventType.Sent:
        return {
          percentage: 1
        };
      default:
        return {
          percentage: 0
        };
    }
  });
}
