import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from './../../auth/permission.guard';
import { CounterListComponent } from './counter-list/counter-list.component';
import { CounterEditorComponent } from './counter-editor/counter-editor.component';
import { P_EDIT_CUSTOM_COMMANDS, P_READ_CUSTOM_COMMANDS } from 'src/app/permissions';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '_/_',
  },
  {
    path: ':messageInterface/:channel',
    component: CounterListComponent,
    canActivate: [PermissionGuard],
    data: {
      permissions: P_READ_CUSTOM_COMMANDS
    }
  },
  {
    path: ':messageInterface/:channel/:command',
    component: CounterEditorComponent,
    canActivate: [PermissionGuard],
    data: {
      permissions: P_EDIT_CUSTOM_COMMANDS
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CounterCommandsRoutingModule { }
