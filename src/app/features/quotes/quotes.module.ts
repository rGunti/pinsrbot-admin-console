import { MomentModule } from 'ngx-moment';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { QuotesRoutingModule } from './quotes-routing.module';
import { QuoteListComponent } from './quote-list/quote-list.component';
import { QuoteEditorComponent } from './quote-editor/quote-editor.component';


@NgModule({
  declarations: [
    QuoteListComponent,
    QuoteEditorComponent
  ],
  imports: [
    CommonModule,
    QuotesRoutingModule,
    MomentModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatExpansionModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatListModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,

    CommonControlsModule,
  ]
})
export class QuotesModule { }
