import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';

import { MomentModule } from 'ngx-moment';

import { CommandListRoutingModule } from './command-list-routing.module';
import { CommandListComponent } from './command-list.component';
import { SyntaxComponent } from './help-dialogs/syntax/syntax.component';
import { PrivilegeComponent } from './help-dialogs/privilege/privilege.component';


@NgModule({
  declarations: [
    CommandListComponent,
    SyntaxComponent,
    PrivilegeComponent
  ],
  imports: [
    CommonModule,
    CommandListRoutingModule,
    MomentModule,
    MatExpansionModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatTooltipModule,
  ]
})
export class CommandListModule { }
