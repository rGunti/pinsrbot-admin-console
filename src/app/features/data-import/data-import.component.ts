import { HttpErrorResponse } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { V2ErrorResponse } from 'src/app/utils/http-error.interceptor';
import { DataImportService } from './data-import.service';
export { V2ErrorResponse } from '../../utils/http-error.interceptor';

@Component({
  selector: 'app-data-import',
  templateUrl: './data-import.component.html',
  styleUrls: ['./data-import.component.scss']
})
export class DataImportComponent implements OnInit {

  isUploading: boolean = true;
  status: string = 'Ready';
  percentage: number = 0;
  simulate: boolean = true;

  private file: File | null = null;

  constructor(
    private readonly dialog: GenericDialogService,
    private readonly dataImport: DataImportService,
  ) { }

  get progressBarMode(): ProgressBarMode {
    return 'determinate';
  }

  @HostListener('change', ['$event.target.files'])
  emitFiles(event: FileList) {
    const file = event && event.item(0);
    this.file = file;
  }

  ngOnInit(): void {
    this.isUploading = false;
  }

  private setStatus(status: string, percentage?: number): void {
    this.status = status;
    if (percentage !== undefined) {
      this.percentage = percentage;
    }
  }

  onImport(): void {
    if (!this.file) {
      return;
    }

    this.isUploading = true;
    this.setStatus('Preparing upload, please wait ...');

    this.dataImport.uploadFile(this.file, this.simulate).subscribe({
      next: (s) => {
        this.setStatus('Uploading file ...', s.percentage);
      },
      error: (e: HttpErrorResponse) => {
        this.isUploading = false;
        this.setStatus('Upload failed');

        if (e.error) {
          const err: V2ErrorResponse = e.error;
          this.dialog.snack('Data Import failed');
          this.setStatus(err.message, 100);
        } else {
          switch (e.status) {
            case 400:
              this.dialog.snack('Your file is either too large or invalid');
              break;
            case 401:
              this.dialog.snack('You are not authorized to import this file');
              break;
            default:
              this.dialog.snack(`File Upload failed with Error Code ${e.status} ${e.statusText} (${e.message})`);
              break;
          }
        }
      },
      complete: () => {
        this.isUploading = false;
        this.setStatus('Import complete!')
        this.dialog.snack('Import complete!');
      }
    });
  }

}
