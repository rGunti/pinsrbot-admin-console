import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';

export interface ShoutoutMessageSettings {
  id: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class SoConfigService extends RestService {

  constructor(http: HttpClient) {
    super(http, '/api/v1/config/so');
  }

  getMessage(): Observable<ShoutoutMessageSettings[]> {
    return this.http.get<ShoutoutMessageSettings[]>(this.getUrl());
  }

  updateMessages(settings: ShoutoutMessageSettings[]): Observable<void> {
    return this.http.post<void>(this.getUrl(), settings);
  }

  updateMessage(settings: ShoutoutMessageSettings): Observable<void> {
    return this.http.post<void>(this.getUrl(`/${settings.id}`), settings);
  }
}
