import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatchNotesComponent } from './patch-notes.component';

const routes: Routes = [{ path: '', component: PatchNotesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatchNotesRoutingModule { }
