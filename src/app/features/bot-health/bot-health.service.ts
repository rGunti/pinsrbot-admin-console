import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export declare type BotHealthObject = { [key: string]: BotHealth };

export interface BotHealth {
  recordedAt: string;
  hostName: string;
  pid: number;
  memoryUsed: number;
  version: string;
  messageInterfaces: BotMessageInterface[];
  uptime: string;
}

export interface BotMessageInterface {
  name: string;
  fullName: string;
  type: string;
  enabled: boolean;
  supportedFeatures: string;
  threadId?: number;
  currentBufferSize?: number;
  maxBufferSize?: number;
}

@Injectable({
  providedIn: 'root'
})
export class BotHealthService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/health/bots`;

  constructor(private readonly http: HttpClient) { }

  getBotHealth(): Observable<BotHealthObject> {
    return this.http.get<BotHealthObject>(`${this.apiServerUrl}`);
  }

  restartInstance(hostName: string, pid: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${hostName}/${pid}`);
  }
}
