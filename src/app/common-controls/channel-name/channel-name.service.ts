import { Injectable } from '@angular/core';

export interface ParsedChannel {
  messageInterface: string;
  channelName: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChannelNameService {

  constructor() { }

  parseChannelId(channelId: string | null | undefined): ParsedChannel | undefined {
    const split = channelId?.split('/') || [];
    if (split.length >= 2) {
      return {
        messageInterface: split[0],
        channelName: split[1],
      };
    } else {
      return undefined;
    }
  }

  getChannelNameFromId(channelId: string | null | undefined): string | undefined {
    return this.parseChannelId(channelId)?.channelName;
  }

  getChannelInterfaceIcon(messageInterface: string | null | undefined): string {
    switch (messageInterface) {
      case 'Discord':
        return 'discord';
      case 'Twitch':
        return 'twitch';
      default:
        return 'chat';
    }
  }

  getChannelInterfaceIconFromId(channelId: string | null | undefined): string {
    return this.getChannelInterfaceIcon(
      this.parseChannelId(channelId)?.messageInterface
    );
  }

}
