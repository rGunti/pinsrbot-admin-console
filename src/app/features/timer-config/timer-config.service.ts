import { map, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestService } from 'src/app/service/rest.service';

export interface TimerMessageConfig {
  id: string;
  messages: TimerMessage[];
  interval: number;
  minMessages: number;
}

interface TimerMessageApiConfig {
  id: string;
  messages: string[];
  interval: number;
  minMessages: number;
}

export interface TimerMessage {
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class TimerConfigService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, '/api/v1/config/timer');
  }

  getConfigs(): Observable<TimerMessageConfig[]> {
    return this.http.get<TimerMessageApiConfig[]>(this.getUrl()).pipe(
      map(a => a.map(c => {
        const config: TimerMessageConfig = {
          id: c.id,
          interval: c.interval,
          messages: c.messages.map(m => ({ message: m })),
          minMessages: c.minMessages
        };
        return config;
      }))
    );
  }

  private updateConfigList(configs: TimerMessageApiConfig[]): Observable<void> {
    return this.http.post<void>(this.getUrl(), configs);
  }

  updateConfigs(configs: TimerMessageConfig[]): Observable<void> {
    return this.updateConfigList(configs.map(c => {
      return {
        id: c.id,
        interval: c.interval,
        messages: c.messages.map(m => m.message),
        minMessages: c.minMessages,
      };
    }))
  }

  updateConfig(config: TimerMessageConfig): Observable<void> {
    const apiConfig: TimerMessageApiConfig = {
      id: config.id,
      interval: config.interval,
      messages: config.messages.map(m => m.message),
      minMessages: config.minMessages,
    };
    return this.http.post<void>(this.getUrl(`/${config.id}`), apiConfig);
  }
}
