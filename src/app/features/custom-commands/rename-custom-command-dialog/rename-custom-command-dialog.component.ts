import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, debounceTime, filter, map, mergeMap, catchError, of, tap, pipe } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { CustomCommand, CustomCommandsService } from '../custom-commands.service';

@Component({
  templateUrl: './rename-custom-command-dialog.component.html',
  styleUrls: ['./rename-custom-command-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RenameCustomCommandDialogComponent {

  form: FormControl<string | null>;

  constructor(
    private readonly dialogRef: MatDialogRef<RenameCustomCommandDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly command: CustomCommand,
    private readonly customCommand: CustomCommandsService,
    private readonly dialog: GenericDialogService,
    fb: FormBuilder,
  ) {
    this.form = fb.control<string>(command.command, [
      Validators.required,
      (c: AbstractControl) => c.value === command.command ? { cannotBeSame: true } : null,
    ]);
  }

  commit(): void {
    this.form.updateValueAndValidity();
    if (this.form.invalid) {
      return;
    }

    const newCommandName = this.form.value!;

    this.customCommand.getCommand(
      this.command.channel,
      newCommandName
    ).pipe(
      map((r) => r.command),
      catchError((e) => of(null))
    ).subscribe({
      next: (commandName: string | null) => {
        if (!commandName) {
          this.dialogRef.close(this.form.value);
        } else {
          this.dialog.notify(
            'Command with this name already exists',
            'Please choose a different name.'
          );
        }
      }
    });
  }

  public static open(
    matDialog: MatDialog,
    command: CustomCommand,
  ): Observable<string | null> {
    return matDialog.open(RenameCustomCommandDialogComponent, {
      data: command
    }).afterClosed();
  }

}
