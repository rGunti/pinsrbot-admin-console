import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';

import { SubAlertsRoutingModule } from './sub-alerts-routing.module';
import { SubAlertsComponent } from './sub-alerts.component';
import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';


@NgModule({
  declarations: [
    SubAlertsComponent
  ],
  imports: [
    CommonModule,
    SubAlertsRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatToolbarModule,

    CommonControlsModule,
  ]
})
export class SubAlertsModule { }
