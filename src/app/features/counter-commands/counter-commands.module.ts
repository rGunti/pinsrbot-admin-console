import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { CounterCommandsRoutingModule } from './counter-commands-routing.module';
import { CounterListComponent } from './counter-list/counter-list.component';
import { CounterEditorComponent } from './counter-editor/counter-editor.component';
import { RenameCounterCommandDialogComponent } from './rename-counter-command-dialog/rename-counter-command-dialog.component';
import { CounterObsoleteComponent } from './counter-obsolete/counter-obsolete.component';

@NgModule({
  declarations: [
    CounterListComponent,
    CounterEditorComponent,
    RenameCounterCommandDialogComponent,
    CounterObsoleteComponent,
  ],
  imports: [
    CommonModule,
    CounterCommandsRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatExpansionModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatListModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatIconModule,

    CommonControlsModule,
  ]
})
export class CounterCommandsModule { }
