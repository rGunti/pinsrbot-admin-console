import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { UserService } from './user.service';
import * as p from './permissions';
import { environment } from 'src/environments/environment';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { PageTitleService } from './service/page-title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAuth0Loading$ = this.auth.isLoading$;

  isLoggedIn = false;
  canShowQuotes = false;
  canShowBotHealth = false;
  canShowCustomCommands = false;
  canShowConfig = false;

  features = environment.features;
  inV2Mode = environment.v2Mode;
  isProd = environment.production;
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);

  title: string = '';
  icon: string | undefined;

  constructor(
    private readonly auth: AuthService,
    private readonly user: UserService,
    private readonly breakpointObserver: BreakpointObserver,
    private readonly pageTitle: PageTitleService,
  ) {
    this.pageTitle.title$.subscribe({
      next: (title) => {
        setTimeout(() => {
          this.title = title;
        });
      }
    });
    this.pageTitle.icon$.subscribe({
      next: (icon) => {
        setTimeout(() => {
          this.icon = icon;
        });
      }
    });
  }

  get isSvgIcon(): boolean {
    return !!this.icon && this.icon.startsWith('svg:');
  }

  get svgIcon(): string {
    if (this.isSvgIcon) {
      return this.icon!.substring('svg:'.length);
    }
    return this.icon || '';
  }

  ngOnInit(): void {
    this.auth.isAuthenticated$.subscribe({
      next: loggedIn => this.isLoggedIn = loggedIn,
    });
    this.user.hasPermission(p.P_READ_QUOTES).subscribe({
      next: hasPerm => this.canShowQuotes = hasPerm,
    });
    this.user.hasPermission(p.P_READ_BOT).subscribe({
      next: hasPerm => this.canShowBotHealth = hasPerm,
    });
    this.user.hasPermission(p.P_READ_CUSTOM_COMMANDS).subscribe({
      next: hasPerm => this.canShowCustomCommands = hasPerm,
    });
    this.user.hasPermission(p.P_READ_CONFIG).subscribe({
      next: hasPerm => this.canShowConfig = hasPerm,
    });
  }

}
