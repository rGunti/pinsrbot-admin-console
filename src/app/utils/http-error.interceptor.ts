import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GenericDialogService } from '../common-controls/generic-dialog/generic-dialog.service';
import { Router } from '@angular/router';

export interface V2ErrorResponse {
  statusCode: number;
  message: string;
  source?: string;
}

const STATUS_CODE_UNAVAILABLE = 590;

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private readonly snackbar: MatSnackBar,
    private readonly dialog: GenericDialogService,
    private readonly router: Router,
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((e: HttpErrorResponse) => {
        if (e.error && e.error.message) {
          const parsedError: V2ErrorResponse = e.error;
          if (e.status === STATUS_CODE_UNAVAILABLE) {
            this.dialog.notify(
              'Unavailable Feature',
              `${parsedError.message}`
            ).then(() => {
              this.router.navigateByUrl('');
            });
          } else {
            this.snackbar.open(`⚠️ ${parsedError.message}`, undefined, {
              duration: 2500
            });
          }
        }
        throw e;
      })
    );
  }
}
