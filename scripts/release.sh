#!/usr/bin/env sh
PACKAGE_VERSION=${CI_COMMIT_TAG}
PACKAGE_REGISTRY_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/pinsrbot-admin-console/${PACKAGE_VERSION}
URL_ARTIFACT=${PACKAGE_REGISTRY_URL}/pinsrbot-admin-console.zip

echo "Uploading artifact ..."
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --upload-file "pinsrbot-admin-console.zip" \
  "${URL_ARTIFACT}"

echo "Creating release ..."
release-cli create \
  --name "Release $PACKAGE_VERSION" \
  --tag-name "${CI_COMMIT_TAG}" \
  --assets-link "{\"name\":\"Admin Console\",\"url\":\"${URL_ARTIFACT}\"}"
