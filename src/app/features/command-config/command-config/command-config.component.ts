import { P_EDIT_CONFIG } from './../../../permissions';
import { Component, OnInit, ViewChild } from '@angular/core';
import { map } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './command-config.component.html',
  styleUrls: ['./command-config.component.scss']
})
export class CommandConfigComponent implements OnInit {

  channelFilter: string = EMPTY_FILTER;

  selectedChannelIndex: number = 0;
  channels: string[] = [];

  hasEditPermission: boolean = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly pageTitle: PageTitleService,
    private readonly userService: UserService,
  ) { }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Command Configuration', 'toggle_on');

    this.userService.hasPermission(P_EDIT_CONFIG).subscribe({
      next: (v) => this.hasEditPermission = v
    });

    this.initFromRoute();
    this.loadChannels();
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
    }
  }

  private loadChannels(): void {
    this.userService.me().pipe(map(m => m.ownerOf)).subscribe({
      next: (channels) => {
        this.channels = channels;
        this.selectedChannelIndex = Math.max(channels.indexOf(this.channelFilter), 0);
      }
    });
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onSelectedChannelChanged(index: number) {
    const channel = this.channels[index]!;
    this.channelFilter = channel;
    this.updateRoute();
  }

}
