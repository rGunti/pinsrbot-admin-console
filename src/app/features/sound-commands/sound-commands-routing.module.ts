import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { P_EDIT_CUSTOM_COMMANDS, P_READ_CUSTOM_COMMANDS } from 'src/app/permissions';
import { SoundBoardComponent } from './sound-board/sound-board.component';
import { SoundCommandEditorComponent } from './sound-command-editor/sound-command-editor.component';
import { SoundCommandListComponent } from './sound-command-list/sound-command-list.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '_/_'
  },
  {
    path: ':messageInterface/:channel',
    component: SoundCommandListComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_READ_CUSTOM_COMMANDS,
    }
  },
  {
    path: ':messageInterface/:channel/:command',
    component: SoundCommandEditorComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_EDIT_CUSTOM_COMMANDS,
    }
  },
  {
    path: ':messageInterface/:channel/_/board',
    component: SoundBoardComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_READ_CUSTOM_COMMANDS,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoundCommandsRoutingModule { }
