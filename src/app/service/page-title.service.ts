import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Observable, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageTitleService {

  private pageTitleObs: Subject<string> = new Subject<string>();
  private pageIconObs: Subject<string | undefined> = new Subject<string | undefined>();

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
  ) { }

  get title$(): Observable<string> {
    return this.pageTitleObs;
  }

  get icon$(): Observable<string | undefined> {
    return this.pageIconObs;
  }

  setPageTitle(pageTitle: string, pageIcon?: string): void {
    this.pageTitleObs.next(pageTitle);
    this.pageIconObs.next(pageIcon);

    this.document.title = `${pageTitle} - FloppyBot Admin Console`;
  }
}
