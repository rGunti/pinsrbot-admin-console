import { TestBed } from '@angular/core/testing';

import { ChannelNameService } from './channel-name.service';

describe('ChannelNameService', () => {
  let service: ChannelNameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChannelNameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
