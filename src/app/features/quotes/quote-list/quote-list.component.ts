import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorDefaultOptions, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, map, tap } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { P_EDIT_QUOTES } from 'src/app/permissions';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { Quote, QuotesService } from '../quotes.service';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './quote-list.component.html',
  styleUrls: ['./quote-list.component.scss']
})
export class QuoteListComponent implements OnInit {

  displayedColumns = [
    'quoteId',
    'quoteText',
    'createdBy',
    'quoteContext',
    'createdAt',
    'functions',
  ];

  channelFilter: string = EMPTY_FILTER;

  dataSourceDict: { [channelId: string]: Observable<MatTableDataSource<Quote> | null> } = {};

  selectedChannelIndex: number = 0;
  channels: string[] = [];
  channels$: Observable<string[]> = of([]);
  quotes$: Observable<Quote[] | null> = of(null);

  hasEditPermission: boolean = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly quotes: QuotesService,
    private readonly users: UserService,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) { }

  get emptyFilter(): string { return EMPTY_FILTER; }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Quotes', 'format_quote');
    this.initFromRoute();
    this.loadChannels();
    this.users.hasPermission(P_EDIT_QUOTES).subscribe({
      next: (hasPerm) => this.hasEditPermission = hasPerm
    });
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel,
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
    }
  }

  loadChannels(): void {
    this.users.me().pipe(
      map((u) => u.ownerOf),
    ).subscribe({
      next: (channels) => {
        this.channels = channels;
        if (this.channelFilter === EMPTY_FILTER) {
          this.loadQuotesForChannel(channels[0]);
        } else {
          const index = channels.indexOf(this.channelFilter);
          if (index < 0) {
            this.selectedChannelIndex = 0;
          } else {
            this.selectedChannelIndex = index;
            this.loadQuotesForChannel(this.channelFilter);
          }
        }
      }
    });
  }

  loadQuotesForChannel(channel: string): void {
    this.dataSourceDict[channel] = this.quotes.getChannelQuotes(channel).pipe(
      map(q => {
        const source = new MatTableDataSource<Quote>(q);
        source.paginator = this.paginator;
        return source;
      })
    );
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  async deleteQuote(quote: Quote): Promise<void> {
    const reply = await this.dialog.ask(
      `Delete Quote #${quote.quoteId}?`,
      `Would you like to delete the quote #${quote.quoteId} from channel ${quote.channel}?`
    );
    if (!reply) {
      return;
    }

    this.quotes.deleteQuote(quote.channel, quote.quoteId).subscribe({
      complete: () => {
        this.dialog.snack('Quote deleted');
        this.loadQuotesForChannel(quote.channel);
      },
    });
  }

  getEditLink(quote: Quote): string[] {
    return ['../../', ...quote.channel.split('/'), `${quote.quoteId}`];
  }

  onSelectedChannelChanged(index: number) {
    const channel = this.channels[index]!;
    this.channelFilter = channel;
    this.updateRoute();
    this.loadQuotesForChannel(channel);
  }

}
