import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';

export interface PatchNote {
  version: string;
  notes: string;
  link: string;
}

@Injectable({
  providedIn: 'root'
})
export class PatchNotesService extends RestService {

  constructor(
    http: HttpClient,
  ) {
    super(http, `/api/patch-notes`);
  }

  getPatchNotes(): Observable<PatchNote[]> {
    return this.http.get<PatchNote[]>(this.getUrl(''));
  }
}
