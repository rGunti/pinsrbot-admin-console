import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { SoundCommand, SoundCommandsService } from './../sound-commands.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, Observable, of, Subscription, tap } from 'rxjs';
import { InvokeSoundCommandEvent, SoundBoardService } from './sound-board.service';
import { HubConnectionState } from '@microsoft/signalr';
import { PageTitleService } from 'src/app/service/page-title.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { FilesService } from '../../file-explorer/files.service';

export interface SoundBoardPayload {
  command: SoundCommand;
  payload: Blob;
  payloadUrl: string;
};

const distinctArray = (value: any, index: number, self: any[]) => {
  return self.indexOf(value) === index;
};

function randomNumber(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min + 1;
}

@Component({
  templateUrl: './sound-board.component.html',
  styleUrls: ['./sound-board.component.scss']
})
export class SoundBoardComponent implements OnInit, OnDestroy {

  channelId: string | null = null;
  commands$: Observable<SoundCommand[]> = of([]);

  soundFiles: Map<string, string> = new Map<string, string>();
  commandMap: Map<string, SoundCommand> = new Map<string, SoundCommand>();

  audioSource: SafeUrl | null = null;
  status: string = 'Initializing ...';
  volume: number = 0.25;
  muted: boolean = false;

  @ViewChild('audio') audioRef?: HTMLElement;

  private sub: Subscription[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly commands: SoundCommandsService,
    private readonly files: FilesService,
    private readonly sanitizer: DomSanitizer,
    private readonly soundBoard: SoundBoardService,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) {
    this.sub.push(
      this.soundBoard.soundCommandInvoked.subscribe({
        next: (s) => this.onSoundCommandInvoked(s)
      })
    );
  }

  get connectionStateIcon(): string {
    switch (this.hubConnectionState) {
      case HubConnectionState.Connected:
        return 'lan-connect';
      case HubConnectionState.Connecting:
      case HubConnectionState.Reconnecting:
        return 'lan-pending';
      case HubConnectionState.Disconnecting:
      case HubConnectionState.Disconnected:
        return 'lan-disconnect';
      default:
        return 'help_outline';
    }
  }

  get hubConnectionState(): HubConnectionState {
    return this.soundBoard.connectionState;
  }

  get isConnecting(): boolean {
    switch (this.hubConnectionState) {
      case HubConnectionState.Connecting:
      case HubConnectionState.Reconnecting:
        return true;
      default:
        return false;
    }
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Sound Board', 'volume_up');

    const {
      messageInterface,
      channel
    } = this.activatedRoute.snapshot.params;
    this.channelId = `${messageInterface}/${channel}`;
    this.loadSoundCommands();
    this.reconnectClicked();
  }

  ngOnDestroy(): void {
    this.soundBoard.disconnect();
  }

  onSoundCommandInvoked(e: InvokeSoundCommandEvent): void {
    this.updateStatus(`Sound Command requested: ${e.commandName} by ${e.invokedBy} at ${e.invokedAt}`);
    this.playCommandByName(e.commandName);
  }

  playCommandByName(commandName: string): void {
    if (!this.commandMap.has(commandName)) {
      this.updateStatus(`Cannot play command ${commandName}, command info not available`);
      return;
    }
    this.playCommand(this.commandMap.get(commandName)!);
  }

  playCommand(command: SoundCommand): void {
    let soundFile: string;
    if (command.payloadId) {
      soundFile = `LEGACY_${command.commandName}`;
    } else if (command.soundFiles) {
      const i = randomNumber(0, command.soundFiles.length);
      soundFile = command.soundFiles[i - 1];
    } else {
      this.updateStatus(`Cannot play command ${command.commandName}, no sound file to play`);
      return;
    }

    if (!this.soundFiles.has(soundFile)) {
      this.updateStatus(`Cannot play command ${command.commandName}, file not loaded (${soundFile})`);
      return;
    }

    const soundFileUrl = this.soundFiles.get(soundFile);
    this.audioSource = this.sanitizer.bypassSecurityTrustUrl(soundFileUrl!);
  }

  stopSound(): void {
    this.audioSource = null;
  }

  private updateStatus(status: string): void {
    setTimeout(() => {
      this.status = status;
    }, 1);
  }

  private loadSoundCommands(): void {
    this.updateStatus('Loading sound commands ...');
    this.commands$ = this.commands.getCommandsForChannel(this.channelId!);

    this.commands$.pipe(
      mergeMap(commands => commands
        .filter(c => c.soundFiles && c.soundFiles.length > 0)
        .flatMap(c => c.soundFiles)
        .filter(distinctArray)
        .map(f => ({
          file: f,
          blob: this.files.downloadFileWithPath(f)
        })))
    ).subscribe({
      next: (s) => {
        s.blob.subscribe({
          next: (blob) => {
            this.soundFiles.set(s.file, URL.createObjectURL(blob));
          },
          complete: () => {
            this.updateStatus(`Loaded ${this.commandMap.size} commands and ${this.soundFiles.size} sound file(s)`);
          }
        })
      }
    });

    this.commands$.pipe(
      mergeMap(commands => commands
        .filter(c => !!c.payloadId)
        .map(c => this.commands.getSoundFile(c.channelId, c.commandName)
          .pipe(
            map(b => ({
              command: c,
              payload: b,
              payloadUrl: URL.createObjectURL(b),
            })),
            tap(s => {
              this.soundFiles.set(`LEGACY_${s.command.commandName}`, s.payloadUrl);
            })
          )))
    ).subscribe({
      next: s => {
        s.subscribe();
      }
    });

    this.commands$.subscribe({
      next: (commands) => {
        this.commandMap.clear();
        for (const command of commands) {
          this.commandMap.set(command.commandName, command);
        }
      }
    });
  }

  async reconnectClicked(): Promise<void> {
    if (this.soundBoard.connectionState === HubConnectionState.Connected) {
      await this.soundBoard.disconnect();
    }

    await this.soundBoard.connect().then(() => {
      this.soundBoard.loginToChannel(this.channelId!);
    });
  }

  formatVolume(volume: number): string {
    return `${Math.round(volume * 100)}%`;
  }

  async goBack(): Promise<void> {
    const reply = await this.dialog.ask(
      'Close Sound Board?',
      'Would you like to close the Sound Board and go back?'
    );
    if (!reply) {
      return;
    }

    this.router.navigate(['../../'], {
      relativeTo: this.activatedRoute
    });
  }

}

