import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimerConfigComponent } from './timer-config/timer-config.component';

const routes: Routes = [{ path: '', component: TimerConfigComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimerConfigRoutingModule { }
