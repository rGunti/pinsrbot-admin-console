import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { CommandConfigRoutingModule } from './command-config-routing.module';
import { CommandConfigComponent } from './command-config/command-config.component';
import { CommandConfigTabComponent } from './command-config-tab/command-config-tab.component';
import { CommandConfigEditorComponent } from './command-config-editor/command-config-editor.component';

@NgModule({
  declarations: [
    CommandConfigComponent,
    CommandConfigTabComponent,
    CommandConfigEditorComponent
  ],
  imports: [
    CommonModule,
    CommandConfigRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatIconModule,
    MatTabsModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,

    CommonControlsModule,
  ]
})
export class CommandConfigModule { }
