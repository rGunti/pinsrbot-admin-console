import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';

import { SoundCommandsRoutingModule } from './sound-commands-routing.module';
import { SoundCommandListComponent } from './sound-command-list/sound-command-list.component';
import { SoundCommandEditorComponent } from './sound-command-editor/sound-command-editor.component';
import { SoundBoardComponent } from './sound-board/sound-board.component';
import { RenameSoundCommandDialogComponent } from './rename-sound-command-dialog/rename-sound-command-dialog.component';


@NgModule({
  declarations: [
    SoundCommandListComponent,
    SoundCommandEditorComponent,
    SoundBoardComponent,
    RenameSoundCommandDialogComponent,
  ],
  imports: [
    CommonModule,
    SoundCommandsRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatCardModule,
    MatExpansionModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatListModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatDialogModule,

    CommonControlsModule,
  ]
})
export class SoundCommandsModule { }
