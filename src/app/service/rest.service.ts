import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export abstract class RestService {

  protected readonly baseUrl: string;

  constructor(
    protected readonly http: HttpClient,
    baseUrl: string,
  ) {
    this.baseUrl = `${environment.api.serverUrl}${baseUrl}`;
  }

  protected getUrl(route: string = ''): string {
    return `${this.baseUrl}${route}`;
  }
}
