import { Observable } from 'rxjs';
import { Component, OnInit, ChangeDetectionStrategy, Inject, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FilesService } from '../files.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss'],
})
export class UploadDialogComponent implements OnInit {

  isUploading: boolean = true;
  status: string = 'Ready';
  percentage: number = 0;

  private file: File | null = null;

  constructor(
    private readonly dialogRef: MatDialogRef<UploadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private readonly channelId: string,
    private readonly files: FilesService,
    private readonly dialog: GenericDialogService,
  ) {
  }

  @HostListener('change', ['$event.target.files'])
  emitFiles( event: FileList ) {
    console.log('Emit Files');
    const file = event && event.item(0);
    this.file = file;
  }

  get hasFiles(): boolean {
    return this.file !== null;
  }

  static open(dialog: MatDialog, channelId: string): Observable<void> {
    return dialog.open(UploadDialogComponent, { data: channelId, disableClose: true }).afterClosed();
  }

  private setStatus(status: string, percentage?: number): void {
    this.status = status;
    if (percentage !== undefined) {
      this.percentage = percentage;
    }
  }

  ngOnInit(): void {
    this.isUploading = false;
  }

  runUpload(): void {
    if (!this.file) {
      return;
    }

    this.isUploading = true;
    this.setStatus('Preparing upload, please wait ...');

    this.files.uploadFile(this.channelId, this.file).subscribe({
      next: (s) => {
        this.setStatus('Uploading file ...');
        this.percentage = s.percentage;
      },
      error: (e) => {
        this.isUploading = false;

        console.log('Upload failed', e);
        switch (e.status) {
          case 400:
            this.dialog.snack('File is too large or missing');
            break;
          case 402:
            this.dialog.snack('You do not have enough storage space to store this file');
            break;
          case 404:
            this.dialog.snack('No permission to upload file to this channel');
            break;
          case 409:
            this.dialog.snack('File with the same name already exists');
            break;
          case 413:
            this.dialog.snack('File is too large to be uploaded');
            break;
          default:
            this.dialog.snack(`File Upload failed with Error Code ${e.status} ${e.statusText} (${e.message})`);
            break;
        }
      },
      complete: () => {
        this.isUploading = false;
        this.dialogRef.close();
      }
    });
  }

}
