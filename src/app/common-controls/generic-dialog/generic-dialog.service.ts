import { GenericDialogResponse, GenericDialogSettings } from './generic-dialog.entities';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GenericDialogComponent } from './generic-dialog.component';
import { lastValueFrom, map, Observable } from 'rxjs';
import { ThemePalette } from '@angular/material/core';

@Injectable({
  providedIn: 'root'
})
export class GenericDialogService {

  constructor(
    private readonly dialog: MatDialog,
    private readonly snackbar: MatSnackBar,
  ) { }

  open(settings: GenericDialogSettings): Observable<GenericDialogResponse> {
    return this.dialog.open(GenericDialogComponent, {
      data: settings
    }).afterClosed().pipe(
      map(r => !!r)
    );
  }

  ask(title: string, message?: string, icon?: string, svg?: boolean, color?: ThemePalette): Promise<boolean> {
    return lastValueFrom(this.open({
      title,
      message,
      icon: icon || 'help_outline',
      useSvgIcon: svg || false,
      type: 'yes-no',
      color: color || 'primary'
    }));
  }

  notify(title: string, message?: string, icon?: string, svg?: boolean): Promise<boolean> {
    return lastValueFrom(this.open({
      title,
      message,
      icon: icon || 'help_outline',
      useSvgIcon: svg || false,
      type: 'ok',
      color: 'primary'
    }));
  }

  snack(message: string, duration: number = 1000): void {
    this.snackbar.open(message, undefined, { duration });
  }
}
