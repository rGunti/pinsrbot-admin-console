import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private readonly userService: UserService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const permissionToHave = route.data['permission'] as string;
    return this.userService.hasPermission(permissionToHave).pipe(
      //tap((hasPerm) => console.log('> Can Activate Permission evaluation:', permissionToHave, hasPerm))
    );
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const permissionToHave = childRoute.data['permission'] as string;
    return this.userService.hasPermission(permissionToHave).pipe(
      //tap((hasPerm) => console.log('> Can Activate Child Permission evaluation:', permissionToHave, hasPerm))
    );
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const permissionToHave = route.data!['permission'] as string;
    return this.userService.hasPermission(permissionToHave).pipe(
      //tap((hasPerm) => console.log('> Can Load Permission evaluation:', permissionToHave, hasPerm))
    );
  }

}
