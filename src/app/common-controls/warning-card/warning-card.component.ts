import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warning-card',
  templateUrl: './warning-card.component.html',
  styleUrls: ['./warning-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WarningCardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
