import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface CustomCommand {
  channel: string;
  command: string;
  response: string;
  responseVariants?: string[];
  limitedToMod: boolean;
  limitedToUsers: string[];
  timeout: number;
}

@Injectable({
  providedIn: 'root'
})
export class CustomCommandsService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/custom-commands`;

  constructor(
    private readonly http: HttpClient
  ) { }

  getCommands(): Observable<CustomCommand[]> {
    return this.http.get<CustomCommand[]>(`${this.apiServerUrl}`);
  }

  getCommandsForChannel(channelId: string): Observable<CustomCommand[]> {
    return this.http.get<CustomCommand[]>(`${this.apiServerUrl}/${channelId}`);
  }

  getCommand(channelId: string, command: string): Observable<CustomCommand> {
    return this.http.get<CustomCommand>(`${this.apiServerUrl}/${channelId}/${command}`);
  }

  createCommand(command: CustomCommand): Observable<void> {
    return this.http.post<void>(`${this.apiServerUrl}/${command.channel}`, command);
  }

  updateCommand(command: CustomCommand): Observable<void> {
    return this.http.put<void>(`${this.apiServerUrl}/${command.channel}/${command.command}`, command);
  }

  deleteCommand(channelId: string, command: string): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${channelId}/${command}`);
  }

  renameCommand(channelId: string, command: string, newCommand: string): Observable<void> {
    return this.http.put<void>(
      `${this.apiServerUrl}/${channelId}/${command}/rename`,
      JSON.stringify(newCommand),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }

  getCounterValue(channelId: string, command: string): Observable<number> {
    return this.http.get<number>(`${this.apiServerUrl}/${channelId}/${command}/counter`);
  }
  updateCounterValue(channelId: string, command: string, newValue: number): Observable<void> {
    return this.http.put<void>(`${this.apiServerUrl}/${channelId}/${command}/counter`, newValue);
  }
  clearCounterValue(channelId: string, command: string): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${channelId}/${command}/counter`);
  }
}
