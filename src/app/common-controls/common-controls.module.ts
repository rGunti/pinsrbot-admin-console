import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChannelNameComponent } from './channel-name/channel-name.component';
import { ChannelIconComponent } from './channel-icon/channel-icon.component';
import { GenericDialogComponent } from './generic-dialog/generic-dialog.component';
import { PermissionIconComponent } from './permission-icon/permission-icon.component';
import { ListPipe } from './list.pipe';
import { ChannelAliasPipe } from './channel-alias.pipe';
import { FileSizePipe } from './file-size.pipe';
import { FileSelectDialogComponent } from './file-select-dialog/file-select-dialog.component';
import { FileSelectInputComponent } from './file-select-input/file-select-input.component';
import { WarningCardComponent } from './warning-card/warning-card.component';


@NgModule({
  declarations: [
    ChannelNameComponent,
    ChannelIconComponent,
    GenericDialogComponent,
    PermissionIconComponent,
    ListPipe,
    ChannelAliasPipe,
    FileSizePipe,
    FileSelectDialogComponent,
    FileSelectInputComponent,
    WarningCardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatTooltipModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule
  ],
  exports: [
    ChannelNameComponent,
    ChannelIconComponent,
    GenericDialogComponent,
    PermissionIconComponent,
    ListPipe,
    ChannelAliasPipe,
    FileSizePipe,
    FileSelectDialogComponent,
    FileSelectInputComponent,
    WarningCardComponent,
  ]
})
export class CommonControlsModule { }
