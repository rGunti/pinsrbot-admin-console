import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { SubAlertConfig, SubAlertsService } from './sub-alerts.service';

@Component({
  selector: 'app-sub-alerts',
  templateUrl: './sub-alerts.component.html',
  styleUrls: ['./sub-alerts.component.scss']
})
export class SubAlertsComponent implements OnInit {

  config$: Observable<SubAlertConfig[] | null> = of(null);
  expandedChannelId: string | undefined;

  constructor(
    private readonly pageTitle: PageTitleService,
    private readonly snackbar: MatSnackBar,
    private readonly subAlerts: SubAlertsService,
  ) {
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Sub Alerts (β)', 'notifications_none');
    this.load();
  }

  trackConfig(index: number, config: any): string {
    return `${index}`;
  }

  load(): void {
    this.config$ = this.subAlerts.getMessages();
  }

  clearMessage(config: SubAlertConfig, prop: keyof SubAlertConfig): void {
    config[prop] = '';
  }

  saveConfig(config: SubAlertConfig): void {
    this.subAlerts.updateMessage(config).subscribe({
      next: () => {
        this.snackbar.open('Sub Alert saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

  saveAllChanges(configs: SubAlertConfig[]): void {
    this.subAlerts.updateMessages(configs).subscribe({
      next: () => {
        this.snackbar.open('Sub Alerts saved', undefined, { duration: 1000 });
        this.load();
      },
      error: (e) => {
        alert(`Failed to save changes: ${e.message}`);
      }
    });
  }

}
