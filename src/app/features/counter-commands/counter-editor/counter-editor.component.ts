import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, debounceTime, filter, map, mergeMap, catchError, tap } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { PageTitleService } from 'src/app/service/page-title.service';
import { CounterCommandStatus, CounterService } from '../counter.service';

const NEW_COMMAND_NAME = '_';

@Component({
  templateUrl: './counter-editor.component.html',
  styleUrls: ['./counter-editor.component.scss']
})
export class CounterEditorComponent implements OnInit {

  command$: Observable<CounterCommandStatus | null> = of(null);
  commandName: string = NEW_COMMAND_NAME;

  form: UntypedFormGroup;

  commandNameDuplicate$: Observable<boolean>;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly command: CounterService,
    private readonly fb: UntypedFormBuilder,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) {
    this.form = fb.group({
      channelId: ['', [Validators.required]],
      name: ['', [
        Validators.required,
        Validators.pattern(`[a-z]{1,}`)
      ]],
      response: ['', [
        Validators.required,
        Validators.maxLength(400),
      ]],
      state: [0, [Validators.required]]
    });

    this.commandNameDuplicate$ = this.form.get('name')!.valueChanges.pipe(
      debounceTime(500),
      filter((commandName: string) => !!commandName && (this.inCreationMode || commandName === this.commandName)),
      map((commandName: string) => commandName.toLowerCase()),
      mergeMap((commandName) => {
        const channel = this.form.get('channelId')!.value;
        return this.command.getCommand(channel, commandName).pipe(
          catchError((e) => of(null)),
        );
      }),
      map((c) => !!c),
      tap((exists) => {
        if (exists) {
          this.form.get('command')!.setErrors({
            commandExists: true
          });
        }
      })
    );
  }

  get inCreationMode(): boolean {
    return this.commandName === NEW_COMMAND_NAME;
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Counter Commands', 'svg:counter');

    const {
      messageInterface,
      channel,
      command
    } = this.activatedRoute.snapshot.params;

    this.commandName = command;
    let command$: Observable<CounterCommandStatus>;
    if (command === NEW_COMMAND_NAME) {
      const newCommand: CounterCommandStatus = {
        channelId: `${messageInterface}/${channel}`,
        name: '',
        response: '',
        state: 0
      };
      command$ = of(newCommand);
      this.pageTitle.setPageTitle(`New Custom Command`, 'border_color');
    } else {
      command$ = this.command.getCommand(`${messageInterface}/${channel}`, command).pipe(
        tap((c) => {
          this.pageTitle.setPageTitle(`Custom Command "${c.name}"`, 'border_color');
        })
      );
    }

    this.command$ = command$.pipe(
      tap((c) => this.updateForm(c))
    );
  }

  private updateForm(command: CounterCommandStatus): void {
    this.form.patchValue(command);
  }

  get responseLength(): number {
    return (this.form.get('response')?.value as string | undefined)?.length || 0;
  }

  saveCommand(command: CounterCommandStatus): void {
    if (!this.form.valid) {
      console.log('Form Errors', this.form);
      alert('Cannot save. Please check your entries and try again.');
      return;
    }

    const newCommand: CounterCommandStatus = {
      ...command,
      ...this.form.value,
    };
    // Force command name to be lower case
    newCommand.name = newCommand.name.toLowerCase();

    if (this.inCreationMode) {
      // Create Command
      this.command.createCommand({
        channelId: newCommand.channelId,
        name: newCommand.name,
        response: newCommand.response
      }).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not create command due to an error: ${e.message}`);
        }
      });
    } else {
      // Update Command
      this.command.updateCommand(newCommand).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not update command due to an error: ${e.message}`);
        }
      });
    }
  }

}
