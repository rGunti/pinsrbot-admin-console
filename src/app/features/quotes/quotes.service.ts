import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface Quote {
  id: string;
  channel: string;
  quoteId: number;
  quoteText: string;
  quoteContext: string;
  createdAt: Date;
  createdBy: string;
}

@Injectable({
  providedIn: 'root'
})
export class QuotesService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/quotes`;

  constructor(
    private readonly http: HttpClient,
  ) { }

  getQuotes(): Observable<Quote[]> {
    return this.http.get<Quote[]>(`${this.apiServerUrl}`);
  }

  getChannelQuotes(channelId: string): Observable<Quote[]> {
    return this.http.get<Quote[]>(`${this.apiServerUrl}/${channelId}`);
  }

  getQuote(messageInterface: string, channel: string, quoteNumber: number | string): Observable<Quote> {
    return this.http.get<Quote>(`${this.apiServerUrl}/${messageInterface}/${channel}/${quoteNumber}`);
  }

  updateQuote(quote: Quote): Observable<void> {
    return this.http.put<void>(`${this.apiServerUrl}/${quote.channel}/${quote.quoteId}`, quote);
  }

  deleteQuote(channelId: string, quoteNumber: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${channelId}/${quoteNumber}`);
  }
}
