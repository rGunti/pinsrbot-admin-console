import { DOCUMENT } from '@angular/common';
import { Component, ChangeDetectionStrategy, Inject, Input } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { map } from 'rxjs';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserMenuComponent {

  @Input() showUserName: boolean = true;

  isAuthenticated$ = this.authService.isAuthenticated$;
  user$ = this.authService.user$;

  constructor(
    private readonly authService: AuthService,
    @Inject(DOCUMENT) private readonly doc: Document,
  ) { }

  login(): void {
    this.authService.loginWithRedirect();
  }

  logout(): void {
    this.authService.logout({ returnTo: this.doc.location.origin });
  }

}
