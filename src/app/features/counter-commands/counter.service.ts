import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';

export interface CounterCommand {
  id?: string | null;
  channelId: string;
  name: string;
  response: string;
}

export interface CounterCommandStatus extends CounterCommand {
  state: number;
}

@Injectable({
  providedIn: 'root'
})
export class CounterService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, `/api/v1/counter-commands/`);
  }

  getCommands(): Observable<CounterCommandStatus[]> {
    return this.http.get<CounterCommandStatus[]>(
      this.getUrl(''));
  }

  getCommandsForChannel(channelId: string): Observable<CounterCommandStatus[]> {
    return this.http.get<CounterCommandStatus[]>(
      this.getUrl(channelId));
  }

  getCommand(channelId: string, commandName: string): Observable<CounterCommandStatus> {
    return this.http.get<CounterCommandStatus>(
      this.getUrl(`${channelId}/${commandName}`));
  }

  createCommand(newCommand: CounterCommand): Observable<void> {
    return this.http.post<void>(
      this.getUrl(`${newCommand.channelId}`),
      newCommand);
  }

  updateCommand(newCommand: CounterCommandStatus): Observable<void> {
    return this.http.put<void>(
      this.getUrl(`${newCommand.channelId}/${newCommand.name}`),
      newCommand);
  }

  deleteCommand(channelId: string, commandName: string): Observable<void> {
    return this.http.delete<void>(
      this.getUrl(`${channelId}/${commandName}`));
  }

  resetCommand(channelId: string, commandName: string): Observable<void> {
    return this.http.delete<void>(
      this.getUrl(`${channelId}/${commandName}/state`));
  }

  renameCommand(channelId: string, commandName: string, newCommandName: string): Observable<void> {
    return this.http.put<void>(
      this.getUrl(`${channelId}/${commandName}/rename`),
      JSON.stringify(newCommandName),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

}
