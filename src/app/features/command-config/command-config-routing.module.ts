import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { P_EDIT_CONFIG } from 'src/app/permissions';
import { CommandConfigEditorComponent } from './command-config-editor/command-config-editor.component';
import { CommandConfigComponent } from './command-config/command-config.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '_/_' },
  { path: ':messageInterface/:channel', component: CommandConfigComponent },
  {
    path: ':messageInterface/:channel/:command',
    component: CommandConfigEditorComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_EDIT_CONFIG
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommandConfigRoutingModule { }
