import { Component, ChangeDetectionStrategy, Input, HostBinding, ViewEncapsulation } from '@angular/core';
import { ChannelNameService } from './../channel-name/channel-name.service';

@Component({
  selector: 'app-channel-icon',
  templateUrl: './channel-icon.component.html',
  styleUrls: ['./channel-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChannelIconComponent {

  @HostBinding('class.mat-icon') matIconClass: boolean = true;

  @Input() channelId?: string;
  @Input() messageInterface?: string;

  constructor(
    public readonly channelNameService: ChannelNameService
  ) { }

}
