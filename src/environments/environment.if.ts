export interface Environment {
  production: boolean;
  auth0: {
    domain: string;
    clientId: string;
    audience: string;
    redirectUri: string;
  };
  api: {
    serverUrl: string;
  };
  v2Mode: boolean;
  features: {
    dataImport: boolean | number;
    dataExport: boolean | number;
  };
}
