import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { AuthModule } from '@auth0/auth0-angular';
import { AccessControlDirective } from './access-control.directive';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AccessControlDirective,
    UserMenuComponent,
  ],
  imports: [
    CommonModule,
    AuthModule,
    RouterModule,

    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatDividerModule,
  ],
  exports: [
    UserMenuComponent,
  ]
})
export class CommonAuthModule { }
