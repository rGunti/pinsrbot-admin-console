import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BotHealthComponent } from './bot-health.component';

const routes: Routes = [{ path: '', component: BotHealthComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BotHealthRoutingModule { }
