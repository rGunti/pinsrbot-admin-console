import { catchError, map, Observable, of } from 'rxjs';
import { CounterCommand, CounterService } from './../counter.service';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  templateUrl: './rename-counter-command-dialog.component.html',
  styleUrls: ['./rename-counter-command-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RenameCounterCommandDialogComponent {

  form: FormControl<string | null>;

  constructor(
    private readonly dialogRef: MatDialogRef<RenameCounterCommandDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly command: CounterCommand,
    private readonly counterCommands: CounterService,
    private readonly dialog: GenericDialogService,
    fb: FormBuilder,
  ) {
    this.form = fb.control<string>(command.name);
  }

  static open(matDialog: MatDialog, command: CounterCommand): Observable<string | null> {
    return matDialog.open(RenameCounterCommandDialogComponent, {
      data: command
    }).afterClosed();
  }

  commit(): void {
    this.form.updateValueAndValidity();
    if (this.form.invalid) {
      return;
    }

    const newCommandName = this.form.value!;
    this.counterCommands.getCommand(
      this.command.channelId,
      newCommandName
    ).pipe(
      map((r) => r.name),
      catchError((e) => of(null))
    ).subscribe({
      next: (commandName: string | null) => {
        if (!commandName) {
          this.dialogRef.close(this.form.value);
        } else {
          this.dialog.notify(
            'Command with this name already exists',
            'Please choose a different name.'
          );
        }
      }
    });
  }

}
