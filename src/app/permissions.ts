export const P_READ_COMMANDS = 'read:commands';
export const P_READ_BOT = 'read:bot';
export const P_RESTART_BOT = 'restart:bot';

export const P_READ_QUOTES = 'read:quotes';
export const P_EDIT_QUOTES = 'edit:quotes';

export const P_READ_CUSTOM_COMMANDS = 'read:custom-commands';
export const P_EDIT_CUSTOM_COMMANDS = 'edit:custom-commands';

export const P_READ_CONFIG = 'read:config';
export const P_EDIT_CONFIG = 'edit:config';

export const P_READ_FILES = 'read:files';
export const P_EDIT_FILES = 'edit:files';
