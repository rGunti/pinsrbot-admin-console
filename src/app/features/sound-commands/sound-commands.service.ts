import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface SoundCommand {
  id?: string;
  commandName: string;
  channelId: string;
  limitedToMod: boolean;
  limitedToUsers: string[];
  hideFromCommandList: boolean;
  cooldown: number;
  response: string;
  payloadId?: string;
  soundFiles: string[];
}

@Injectable({
  providedIn: 'root'
})
export class SoundCommandsService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/sound-commands`;

  constructor(
    private readonly http: HttpClient,
  ) { }

  getCommands(): Observable<SoundCommand[]> {
    return this.http.get<SoundCommand[]>(`${this.apiServerUrl}`);
  }

  getCommandsForChannel(channelId: string): Observable<SoundCommand[]> {
    return this.http.get<SoundCommand[]>(`${this.apiServerUrl}/${channelId}`);
  }

  getCommand(channelId: string, command: string): Observable<SoundCommand> {
    return this.http.get<SoundCommand>(`${this.apiServerUrl}/${channelId}/${command}`);
  }

  createCommand(command: SoundCommand): Observable<void> {
    return this.http.post<void>(`${this.apiServerUrl}/${command.channelId}`, command);
  }

  updateCommand(command: SoundCommand): Observable<void> {
    return this.http.put<void>(`${this.apiServerUrl}/${command.channelId}/${command.commandName}`, command);
  }

  deleteCommand(channelId: string, command: string): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/${channelId}/${command}`);
  }

  uploadSoundFile(channelId: string, command: string, file: File): Observable<void> {
    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<void>(
      `${this.apiServerUrl}/${channelId}/${command}/payload`,
      formData);
  }

  getSoundFile(channelId: string, command: string): Observable<Blob> {
    return this.http.get(`${this.apiServerUrl}/${channelId}/${command}/payload`, {
      responseType: 'arraybuffer'
    }).pipe(
      map(a => [new Uint8Array(a, 0, a.byteLength)]),
      map(b => new Blob(b)),
    );
  }

  renameCommand(channelId: string, commandName: string, newCommandName: string): Observable<void> {
    return this.http.put<void>(
      `${this.apiServerUrl}/${channelId}/${commandName}/rename`,
      JSON.stringify(newCommandName),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }
}
