import { map, Observable, of } from 'rxjs';
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { ChannelAliases, UserService } from 'src/app/user.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

@Component({
  selector: 'app-channel-alias-editor',
  templateUrl: './channel-alias-editor.component.html',
  styleUrls: ['./channel-alias-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChannelAliasEditorComponent implements OnInit {

  aliasForm$: Observable<UntypedFormGroup | null> = of(null);

  constructor(
    private readonly userService: UserService,
    private readonly fb: UntypedFormBuilder,
    private readonly dialog: GenericDialogService,
  ) {
  }

  ngOnInit(): void {
    this.load();
  }

  private load(): void {
    this.aliasForm$ = this.userService.me().pipe(
      map(user => user.channelAliases),
      map(aliases => {
        const f = this.fb.group({});
        Object.keys(aliases).sort().forEach(channelId => {
          f.addControl(channelId, this.fb.control(aliases[channelId], [
            Validators.maxLength(64)
          ]))
        });
        return f;
      })
    );
  }

  clearAlias(aliasForm: UntypedFormGroup, channelId: string): void {
    aliasForm.get(channelId)!.setValue('');
  }

  getChannelIds(aliasForm: UntypedFormGroup): string[] {
    return Object.keys(aliasForm.controls);
  }

  saveAliases(aliasForm: UntypedFormGroup): void {
    if (aliasForm.invalid) {
      this.dialog.snack('Check your aliases and try again');
      return;
    }
    this.dialog.snack('Saving aliases ...');
    const aliases: ChannelAliases = aliasForm.value;
    this.userService.updateChannelAliases(aliases).subscribe({
      next: () => {
        this.dialog.snack('Aliases saved');
        this.load();
      },
      error: () => {
        this.dialog.snack('Failed to save aliases');
      }
    });
  }

  async clearForm(): Promise<void> {
    if (!(await this.dialog.ask('Discard all changes', 'Would you like to discard all changes?'))) {
      return;
    }
    this.load();
  }

}
