(function () {
  window['env'] = {
    production: true,
    auth0: {
      domain: '${AUTH0_DOMAIN}',
      clientId: '${AUTH0_CLIENT_ID}',
      audience: '${AUTH0_AUDIENCE}',
      redirectUri: '${AUTH0_CALLBACK_URL}',
    },
    api: {
      serverUrl: '${API_SERVER_URL}',
    },
    v2Mode: !!'${V2_MODE}',
    features: {
      dataImport: !!'${FEATURE_DATA_IMPORT}',
      dataExport: !!'${FEATURE_DATA_EXPORT}'
    },
  };
})();
