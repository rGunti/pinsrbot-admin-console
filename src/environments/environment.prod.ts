import { Environment } from "./environment.if";

export const environment: Environment = (window as any)['env'] as Environment;
