import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface Command {
  name: string;
  obsolete: boolean;
  description: string;
  aliases: string[];
  restrictedToInterfaces: string[];
  requiredPrivilegeLevel: string;
  customCommand: boolean;
  cooldown: CommandCooldown;
  syntax: CommandSyntaxDescription[];
}

export interface CommandCooldown {
  cooldownMode: string;
  defaultCooldown: number;
  modCooldown?: number;
  adminCooldown?: number;
}

export interface CommandSyntaxDescription {
  syntax: string;
  purpose: string;
  examples?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class CommandListService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/commands`;

  constructor(
    private readonly http: HttpClient
  ) { }

  getCommands(): Observable<Command[]> {
    return this.http.get<Command[]>(this.apiServerUrl);
  }
}
