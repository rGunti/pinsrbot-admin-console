import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catchError, map, Observable, of } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { SoundCommand, SoundCommandsService } from '../sound-commands.service';

@Component({
  templateUrl: './rename-sound-command-dialog.component.html',
  styleUrls: ['./rename-sound-command-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RenameSoundCommandDialogComponent {

  form: FormControl<string | null>;

  constructor(
    private readonly dialogRef: MatDialogRef<RenameSoundCommandDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public readonly command: SoundCommand,
    private readonly soundCommand: SoundCommandsService,
    private readonly dialog: GenericDialogService,
    fb: FormBuilder,
  ) {
    this.form = fb.control<string>(command.commandName);
  }

  static open(matDialog: MatDialog, command: SoundCommand): Observable<string | null> {
    return matDialog.open(RenameSoundCommandDialogComponent, {
      data: command
    }).afterClosed();
  }

  commit(): void {
    this.form.updateValueAndValidity();
    if (this.form.invalid) {
      return;
    }

    const newCommandName = this.form.value!;
    this.soundCommand.getCommand(
      this.command.channelId,
      newCommandName
    ).pipe(
      map((r) => r.commandName),
      catchError((e) => of(null))
    ).subscribe({
      next: (commandName: string | null) => {
        if (!commandName) {
          this.dialogRef.close(this.form.value);
        } else {
          this.dialog.notify(
            'Command with this name already exists',
            'Please choose a different name.'
          );
        }
      }
    });
  }

}
