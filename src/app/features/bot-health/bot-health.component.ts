import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { of, map, catchError } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { P_RESTART_BOT } from 'src/app/permissions';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { BotHealth, BotHealthObject, BotHealthService } from './bot-health.service';

export interface BotHealthItem {
  key: string;
  instance: BotHealth;
}

@Component({
  selector: 'app-bot-health',
  templateUrl: './bot-health.component.html',
  styleUrls: ['./bot-health.component.scss']
})
export class BotHealthComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = [
    'key',
    'instance.version',
    'instance.memoryUsed',
    'instance.messageInterfaces',
    'instance.uptime',
    'instance.recordedAt',
    'functions',
  ];

  dataSource: MatTableDataSource<BotHealthItem> | never[] = new MatTableDataSource<BotHealthItem>([]);

  hasRestartPermission = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private readonly api: BotHealthService,
    private readonly title: PageTitleService,
    private readonly dialog: GenericDialogService,
    private readonly user: UserService,
  ) { }

  ngOnInit(): void {
    this.title.setPageTitle('Bot Health', 'health_and_safety');
    this.user.hasPermission(P_RESTART_BOT).subscribe({
      next: (b) => {
        this.hasRestartPermission = b;
      }
    });
  }

  ngAfterViewInit(): void {
    this.load();
  }

  load(): void {
    this.api.getBotHealth().pipe(
      map(i => Object.keys(i).map(key => ({
        key,
        instance: i[key],
      }))),
      map(i => {
        const source = new MatTableDataSource<BotHealthItem>(i);
        source.paginator = this.paginator;
        return source;
      }),
      catchError((e, _) => of([]))
    ).subscribe({
      next: (dataSource) => {
        this.dataSource = dataSource;
      }
    });
  }

  async onRestartInstance(botInstance: BotHealthItem): Promise<void> {
    console.log('Selected instance', botInstance);
    if (!(await this.dialog.ask(
      'Restart Instance?',
      `Would you like to restart the bot instance ${botInstance.key}?`
    ))) {
      return;
    }

    this.api.restartInstance(botInstance.instance.hostName, botInstance.instance.pid).subscribe({
      next: () => {
        this.dialog.snack('Instance restart invoked, it should return shortly ...');
      },
      error: () => {
        this.dialog.snack('Failed to restart instance');
      }
    })
  }

}
