import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { PatchNote, PatchNotesService } from './patch-notes.service';

@Component({
  selector: 'app-patch-notes',
  templateUrl: './patch-notes.component.html',
  styleUrls: ['./patch-notes.component.scss']
})
export class PatchNotesComponent implements OnInit {

  patchNotes$: Observable<PatchNote[]> = of([]);

  constructor(
    private readonly pageTitle: PageTitleService,
    private readonly patchNotes: PatchNotesService,
  ) { }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Patch Notes', 'update');
    this.patchNotes$ = this.patchNotes.getPatchNotes();
  }

}
