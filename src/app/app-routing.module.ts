import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { PermissionGuard } from './auth/permission.guard';
import * as p from './permissions';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () =>
      import('./features/home/home.module').then(m => m.HomeModule),
  },
  { path: 'callback', children: [] },
  { path: 'debug', loadChildren: () => import('./features/debug/debug.module').then(m => m.DebugModule) },
  {
    path: 'profile',
    loadChildren: () => import('./features/profile/profile.module').then(m => m.ProfileModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'bot-health',
    loadChildren: () => import('./features/bot-health/bot-health.module').then(m => m.BotHealthModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_BOT
    }
  },
  {
    path: 'command-list',
    loadChildren: () => import('./features/command-list/command-list.module').then(m => m.CommandListModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_COMMANDS
    }
  },
  {
    path: 'quotes',
    loadChildren: () => import('./features/quotes/quotes.module').then(m => m.QuotesModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_QUOTES
    }
  },
  {
    path: 'custom-commands',
    loadChildren: () => import('./features/custom-commands/custom-commands.module').then(m => m.CustomCommandsModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CUSTOM_COMMANDS
    }
  },
  {
    path: 'sound-commands',
    loadChildren: () => import('./features/sound-commands/sound-commands.module').then(m => m.SoundCommandsModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CUSTOM_COMMANDS,
    }
  },
  {
    path: 'counter-commands',
    loadChildren: () => import('./features/counter-commands/counter-commands.module').then(m => m.CounterCommandsModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CUSTOM_COMMANDS,
    }
  },
  {
    path: 'so-config',
    loadChildren: () => import('./features/so-config/so-config.module').then(m => m.SoConfigModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CONFIG,
    }
  },
  {
    path: 'timer-config',
    loadChildren: () => import('./features/timer-config/timer-config.module').then(m => m.TimerConfigModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CONFIG,
    }
  },
  {
    path: 'command-config',
    loadChildren: () => import('./features/command-config/command-config.module').then(m => m.CommandConfigModule),
    canActivate: [AuthGuard],
    canLoad: [PermissionGuard],
    data: {
      permission: p.P_READ_CONFIG,
    }
  },
  {
    path: 'patch-notes',
    loadChildren: () => import('./features/patch-notes/patch-notes.module').then(m => m.PatchNotesModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
  },
  {
    path: 'file-explorer',
    loadChildren: () => import('./features/file-explorer/file-explorer.module').then(m => m.FileExplorerModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    data: {
      permission: p.P_READ_FILES,
    }
  },
  {
    path: 'sub-alerts',
    loadChildren: () => import('./features/sub-alerts/sub-alerts.module').then(m => m.SubAlertsModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    data: {
      permission: p.P_READ_CONFIG,
    }
  },
  {
    path: 'data-export',
    loadChildren: () => import('./features/data-export/data-export.module').then(m => m.DataExportModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    data: {
      permission: p.P_READ_BOT,
    }
  },
  {
    path: 'data-import',
    loadChildren: () => import('./features/data-import/data-import.module').then(m => m.DataImportModule),
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    data: {
      permission: p.P_READ_BOT
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
