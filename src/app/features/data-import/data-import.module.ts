import { DataImportComponent } from './data-import.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonControlsModule } from 'src/app/common-controls/common-controls.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DataImportRoutingModule } from './data-import-routing.module';


@NgModule({
  declarations: [
    DataImportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatDialogModule,
    MatCheckboxModule,

    CommonControlsModule,
    DataImportRoutingModule
  ]
})
export class DataImportModule { }
