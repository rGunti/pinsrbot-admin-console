import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';

export interface SubAlertConfig {
  id: string;
  message: string;
  reSubMessage: string;
  communitySubMessage: string;
  giftSubMessage: string;
}

@Injectable({
  providedIn: 'root'
})
export class SubAlertsService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, '/api/v1/config/sub-alerts');
  }

  getMessages(): Observable<SubAlertConfig[]> {
    return this.http.get<SubAlertConfig[]>(this.getUrl());
  }

  updateMessages(config: SubAlertConfig[]): Observable<void> {
    return this.http.post<void>(this.getUrl(), config);
  }

  updateMessage(config: SubAlertConfig): Observable<void> {
    return this.http.post<void>(this.getUrl(`/${config.id}`), config);
  }
}
