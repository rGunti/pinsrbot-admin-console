import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder, HubConnectionState } from '@microsoft/signalr';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface InvokeSoundCommandEvent {
  invokedBy: string;
  invokedFrom: string;
  commandName: string;
  invokedAt: Date;
}

@Injectable({
  providedIn: 'root'
})
export class SoundBoardService {

  private subscribedChannel: string | undefined;

  private hub: HubConnection;
  private subject = new Subject<InvokeSoundCommandEvent>();

  constructor() {
    this.hub = new HubConnectionBuilder()
      .withUrl(`${environment.api.serverUrl}/hub/sound-command`)
      .withAutomaticReconnect()
      .build();
  }

  async connect(): Promise<void> {
    this.setupConnection();
    await this.hub.start();
  }

  async disconnect(): Promise<void> {
    await this.hub.stop();
  }

  get connectionState(): HubConnectionState {
    return this.hub.state;
  }

  get soundCommandInvoked(): Observable<InvokeSoundCommandEvent> {
    return this.subject;
  }

  private setupConnection(): void {
    this.hub.onclose((e) => {
    });
    this.hub.onreconnecting((e) => {
    });
    this.hub.onreconnected((s) => {
      if (this.subscribedChannel) {
        this.loginToChannel(this.subscribedChannel);
      }
    });

    this.hub.on('SoundCommandInvoked', (e: InvokeSoundCommandEvent) => {
      this.subject.next(e);
    });
  }

  async loginToChannel(channelId: string): Promise<void> {
    if (this.subscribedChannel) {
      await this.logoutToChannel(channelId);
    }

    await this.hub.invoke('Login', channelId);
    this.subscribedChannel = channelId;
  }

  async logoutToChannel(channelId: string): Promise<void> {
    await this.hub.send('Logout', channelId);
    this.subscribedChannel = undefined;
  }

}
