import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, tap } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { PageTitleService } from 'src/app/service/page-title.service';
import { CommandConfigService, CommandConfiguration, CommandConfigurationDto } from '../command-config.service';

interface CommandConfigForm {
  enabled: boolean;
  config: CommandConfiguration;
}

@Component({
  templateUrl: './command-config-editor.component.html',
  styleUrls: ['./command-config-editor.component.scss']
})
export class CommandConfigEditorComponent implements OnInit {

  form: UntypedFormGroup;

  commandName!: string;
  channelId!: string;
  command$: Observable<CommandConfigurationDto | null> = of(null);

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly configService: CommandConfigService,
    private readonly dialog: GenericDialogService,
    private readonly pageTitle: PageTitleService,
    fb: UntypedFormBuilder,
  ) {
    this.form = fb.group({
      enabled: [true, [Validators.required]],
      config: fb.group({
        disabled: [false],
        channelId: ['', [Validators.required]],
        commandName: ['', [Validators.required]],
        privilegeLevelOverride: [null, []],
        cooldown: [false, [Validators.required]],
        cooldownConfig: fb.group({
          cooldown: [0, []],
          modCooldown: [0, []],
          adminCooldown: [0, []],
        })
      }),
    });
    this.pageTitle.setPageTitle('Configure Command', 'toggle_on');
  }

  get commandEnabled(): boolean {
    return this.form.get('enabled')!.value;
  }

  get cooldownEnabled(): boolean {
    return this.form.get('config.cooldown')!.value;
  }

  ngOnInit(): void {
    const {
      messageInterface,
      channel,
      command
    } = this.activatedRoute.snapshot.params;
    this.pageTitle.setPageTitle(`Configure Command "${command}"`, 'toggle_on');

    this.commandName = command;
    this.channelId = `${messageInterface}/${channel}`;
    this.command$ = this.configService.getConfig(messageInterface, channel, command).pipe(
      tap((c) => {
        this.updateForm({
          enabled: !c.config!.disabled,
          config: c.config!
        });
      })
    );
  }

  private updateForm(config: CommandConfigForm): void {
    this.form.patchValue(config);
    this.onDisableChanged();
    this.onCooldownDisableChanged();
  }

  onDisableChanged(): void {
    const configForm = this.form.get('config')!;
    if (this.commandEnabled) {
      configForm.enable();
      this.onCooldownDisableChanged();
    } else {
      configForm.disable();
    }
  }

  onCooldownDisableChanged(): void {
    const configForm = this.form.get('config.cooldownConfig')!;
    if (this.commandEnabled && this.cooldownEnabled) {
      configForm.enable();
    } else {
      configForm.disable();
    }
  }

  saveChanges(command: CommandConfigurationDto): void {
    if (this.form.invalid) {
      return;
    }

    const formData: CommandConfigForm = this.form.value;
    const config: CommandConfiguration = {
      ...command.config,
      ...formData.config,
      disabled: !formData.enabled
    };
    this.configService.updateConfig(this.channelId, config).subscribe({
      next: () => {
        this.dialog.snack('Configuration updated');
        this.router.navigate(['../'], {
          relativeTo: this.activatedRoute
        });
      },
      error: () => {
        this.dialog.snack('Failed to update configuration');
      }
    });
  }

}
