#!/usr/bin/env sh
P_DOCKERFILE=${1}
P_ARTIFACT=${2}
P_UNPACK_PATH=${3}
P_IMAGE_SUFFIX=${4}
P_IMAGE_ARTIFACT=${5}

IMAGE_NAME="${CI_REGISTRY_IMAGE}${P_IMAGE_SUFFIX}"

set -e

echo "Unpacking ${P_ARTIFACT} to ${P_UNPACK_PATH} ..."
mkdir -p "${P_UNPACK_PATH}"
unzip "${P_ARTIFACT}" -d "${P_UNPACK_PATH}"

echo "Building Docker image ${IMAGE_NAME}:commit-${CI_COMMIT_SHORT_SHA}"
docker build --pull -f "${P_DOCKERFILE}" \
  -t "${IMAGE_NAME}:commit-${CI_COMMIT_SHORT_SHA}" \
  .
if [ "$CI_COMMIT_BRANCH" = "master" ]; then
  echo "Building Branch Docker image ${IMAGE_NAME}:master"
  docker build -f "${P_DOCKERFILE}" \
    -t "${IMAGE_NAME}:master" \
    .
fi
if [ "$CI_COMMIT_TAG" ]; then
  # Parse Version out
  . scripts/semver.sh
  V_MAJOR=0
  V_MINOR=0
  V_PATCH=0
  V_SPECIAL=""
  semverParseInto "$CI_COMMIT_TAG" V_MAJOR V_MINOR V_PATCH V_SPECIAL

  echo "Building Release Docker image ${IMAGE_NAME}:${CI_COMMIT_TAG}"
  docker build -f "${P_DOCKERFILE}" \
    -t "${IMAGE_NAME}:latest" \
    -t "${IMAGE_NAME}:${CI_COMMIT_TAG}" \
    -t "${IMAGE_NAME}:${V_MAJOR}.${V_MINOR}" \
    .
fi

echo "Exporting Docker image ${IMAGE_NAME} to ${P_IMAGE_ARTIFACT} ..."
docker image save -o "${P_IMAGE_ARTIFACT}.tar" "${IMAGE_NAME}"
