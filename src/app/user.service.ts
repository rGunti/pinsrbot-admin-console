import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, tap, of } from 'rxjs';
import { environment } from 'src/environments/environment';

export type ChannelAliases = { [channelId: string]: string };

export interface User {
  id: string;
  ownerOf: string[];
  channelAliases: ChannelAliases;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly apiServerUrl = `${environment.api.serverUrl}/api/v1/user`;
  private userPermissions: string[] | undefined = undefined;

  constructor(private readonly http: HttpClient) { }

  me(): Observable<User> {
    return this.http.get<User>(`${this.apiServerUrl}/me`);
  }

  myPermissions(): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/me/permissions`).pipe(
      tap((p) => {
        this.userPermissions = p;
      })
    );
  }

  hasPermission(perm: string): Observable<boolean> {
    let baseObs: Observable<string[]>;
    if (this.userPermissions === undefined) {
      baseObs = this.myPermissions();
    } else {
      baseObs = of(this.userPermissions);
    }
    return baseObs.pipe(
      map((p) => p.indexOf(perm) >= 0)
    );
  }

  updateChannelAliases(aliases: ChannelAliases): Observable<void> {
    return this.http.put<void>(`${this.apiServerUrl}/me/channels`, aliases);
  }

  getChannelAlias(channelId: string): Observable<string> {
    return this.http.get<{ alias: string }>(`${this.apiServerUrl}/me/channels/${channelId}`).pipe(
      map(r => r.alias)
    );
  }
}
