import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { P_EDIT_QUOTES } from 'src/app/permissions';
import { QuoteEditorComponent } from './quote-editor/quote-editor.component';
import { QuoteListComponent } from './quote-list/quote-list.component';

const routes: Routes = [
  { path: '', redirectTo: '_/_', pathMatch: 'full' },
  { path: ':messageInterface/:channel', component: QuoteListComponent },
  {
    path: ':messageInterface/:channel/:quoteNumber',
    component: QuoteEditorComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: P_EDIT_QUOTES,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotesRoutingModule { }
