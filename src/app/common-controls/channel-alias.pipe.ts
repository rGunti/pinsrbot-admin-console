import { catchError, map, Observable, of } from 'rxjs';
import { Pipe, PipeTransform } from '@angular/core';
import { UserService } from '../user.service';
import { ChannelNameService } from './channel-name/channel-name.service';

@Pipe({
  name: 'channelAlias'
})
export class ChannelAliasPipe implements PipeTransform {

  constructor(
    private readonly channelNameService: ChannelNameService,
    private readonly userService: UserService,
  ) {}

  transform(
    channelId: string | null | undefined,
    resolve: boolean = false
  ): Observable<string | null | undefined> {
    if (!channelId) {
      return of(channelId);
    }
    const channelName = this.channelNameService.getChannelNameFromId(channelId);
    if (!resolve) {
      return of(channelName);
    }
    return this.userService.getChannelAlias(channelId).pipe(
      map((alias) => alias || channelName),
      catchError(e => {
        console.error('Failed to resolve channel alias', e, channelId);
        return of(channelName);
      })
    );
  }

}
