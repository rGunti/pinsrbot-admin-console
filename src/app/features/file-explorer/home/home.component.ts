import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of, map } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';

const EMPTY_FILTER = '_/_';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  channelFilter: string = EMPTY_FILTER;
  channels$: Observable<string[]> = of([]);

  constructor(
    private readonly pageTitle: PageTitleService,
    private readonly users: UserService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('File Explorer', 'folder_open');
    this.initFromRoute();
    this.loadChannels();
  }

  loadChannels(): void {
    this.channels$ = this.users.me().pipe(
      map((u) => u.ownerOf.sort())
    );
  }

  private initFromRoute(): void {
    const {
      messageInterface,
      channel,
    } = this.activatedRoute.snapshot.params;
    if (!!messageInterface && !!channel) {
      this.channelFilter = `${messageInterface}/${channel}`;
    }
  }

  private updateRoute(): void {
    if (this.channelFilter !== EMPTY_FILTER) {
      this.router.navigate(['../../', ...this.channelFilter.split('/')], {
        relativeTo: this.activatedRoute
      });
    } else {
      this.router.navigate(['../../'], {
        relativeTo: this.activatedRoute
      });
    }
  }

  onPanelOpened(channel: string): void {
    this.channelFilter = channel;
    this.updateRoute();
  }

  onPanelClosed(channel: string): void {
    this.channelFilter = EMPTY_FILTER;
    this.updateRoute();
  }

}
