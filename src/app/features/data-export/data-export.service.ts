import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { RestService } from 'src/app/service/rest.service';

@Injectable({
  providedIn: 'root'
})
export class DataExportService extends RestService {

  constructor(http: HttpClient) {
    super(http, '/api/v1/export');
  }

  downloadExport(channelIds: string[]): Observable<Blob> {
    return this.http.post(this.getUrl('/'), channelIds, {
      responseType: 'arraybuffer'
    }).pipe(
      map(a => [new Uint8Array(a, 0, a.byteLength)]),
      map(b => new Blob(b)),
    );
  }
}
