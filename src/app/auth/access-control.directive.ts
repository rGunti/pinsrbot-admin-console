import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { forkJoin, Observable, of, map, tap } from 'rxjs';
import { UserService } from '../user.service';

@Directive({
  selector: '[appAccessControl]'
})
export class AccessControlDirective implements OnInit {

  @Input('requiredPermission') permission: string | undefined;

  private originalState: string | null = null;

  constructor(
    private readonly elementRef: ElementRef,
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) { }

  private get isLoggedIn(): Observable<boolean> {
    return this.authService.isAuthenticated$;
  }

  private get hasAccess(): Observable<boolean> {
    if (this.permission !== undefined) {
      return this.userService.hasPermission(this.permission);
    }
    return of(false);
  }

  ngOnInit(): void {
    this.originalState = this.elementRef.nativeElement.style.display;
    this.elementRef.nativeElement.style.display = 'none';
    this.checkAccess();
  }

  private checkAccess(): void {
    forkJoin([
      this.isLoggedIn,
      this.hasAccess,
    ]).pipe(
      tap((reply) => { console.log('> Is logged in and has permission', reply, this.permission); }),
      map(([loggedIn, hasAccess]) => loggedIn && hasAccess)
    ).subscribe({
      next: hasPerm => {
        this.elementRef.nativeElement.style.display = hasPerm ? (this.originalState || 'block') : 'none';
      }
    });
  }

}
