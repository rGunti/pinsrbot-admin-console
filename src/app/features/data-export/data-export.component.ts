import { Component, OnInit } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { map, Observable, of } from 'rxjs';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';
import { PageTitleService } from 'src/app/service/page-title.service';
import { UserService } from 'src/app/user.service';
import { DataExportService } from './data-export.service';

@Component({
  selector: 'app-data-export',
  templateUrl: './data-export.component.html',
  styleUrls: ['./data-export.component.scss']
})
export class DataExportComponent implements OnInit {

  channels$: Observable<string[]> = of([]);
  selectedChannels: Set<string> = new Set([]);

  constructor(
    private readonly pageTitle: PageTitleService,
    private readonly users: UserService,
    private readonly exportService: DataExportService,
    private readonly genericDialog: GenericDialogService,
  ) { }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Data Exporter', 'svg:database-export');
    this.loadChannels();
  }

  loadChannels(): void {
    this.channels$ = this.users.me().pipe(
      map((u) => u.ownerOf.sort())
    );
  }

  onTickChange($event: MatCheckboxChange, channel: string): void {
    if ($event.checked) {
      this.selectedChannels.add(channel);
    } else {
      this.selectedChannels.delete(channel);
    }
  }

  onExport(): void {
    this.exportService.downloadExport([...this.selectedChannels])
      .subscribe({
        next: (f) => {
          let url = window.URL.createObjectURL(f);
          let a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = url;
          a.download = 'DataExport.flpyb0t';
          a.click();
          window.URL.revokeObjectURL(url);
          a.remove();
        },
        error: (e) => {
          this.genericDialog.snack('Failed to download file');
        }
      });
  }

}
