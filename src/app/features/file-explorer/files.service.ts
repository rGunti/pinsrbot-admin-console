import { map, Observable } from 'rxjs';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestService } from 'src/app/service/rest.service';
import { StatusReport, uploadProgress } from 'src/app/utils/operators';

export interface FileHeader {
  id: string;
  channelId: string;
  fileName: string;
  fileSize: number;
  mimeType: string;
}

export interface Quota {
  channelId: string;
  maxStorageQuota: number;
  maxFileNumber: number;
  storageUsed: number;
  fileCount: number;
}

@Injectable({
  providedIn: 'root'
})
export class FilesService extends RestService {

  constructor(
    http: HttpClient
  ) {
    super(http, `/api/v1/files`);
  }

  uploadFile(channelId: string, file: File): Observable<StatusReport> {
    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<void>(this.getUrl(`/${channelId}`), formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      uploadProgress()
    );
  }

  listFiles(channelId: string): Observable<FileHeader[]> {
    return this.http.get<FileHeader[]>(this.getUrl(`/${channelId}`));
  }

  deleteFile(channelId: string, fileName: string): Observable<void> {
    return this.http.delete<void>(this.getUrl(`/${channelId}/${fileName}`));
  }

  getQuota(channelId: string): Observable<Quota> {
    return this.http.get<Quota>(this.getUrl(`/${channelId}/quota`));
  }

  downloadFile(channelId: string, fileName: string): Observable<Blob> {
    return this.http.get(this.getUrl(`/${channelId}/dl/${fileName}`), {
      responseType: 'arraybuffer'
    }).pipe(
      map(a => [new Uint8Array(a, 0, a.byteLength)]),
      map(b => new Blob(b)),
    );
  }

  downloadFileWithPath(fullPath: string): Observable<Blob> {
    const split = fullPath.split('/');
    return this.downloadFile(`${split[0]}/${split[1]}`, split[2]);
  }

  getFileIcon(file: FileHeader): string {
    const mime = file.mimeType.toLowerCase().split('/');

    switch (mime[0]) {
      case 'image':
        return 'file-image';
      case 'audio':
        return 'music';
      case 'text':
        return 'file-document-outline';
      case 'application':
        switch (mime[1]) {
          case 'zip':
          case 'vnd.rar':
          case 'x-7z-compressed':
            return 'archive-outline';
          default:
            return 'file-question-outline';
        }
      default:
        return 'file-question-outline';
    }
  }
}
