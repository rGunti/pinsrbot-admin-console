import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, debounceTime, filter, map, mergeMap, Observable, of, tap } from 'rxjs';
import { PageTitleService } from 'src/app/service/page-title.service';
import { SoundCommand, SoundCommandsService } from '../sound-commands.service';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

const NEW_COMMAND_NAME = '_';

@Component({
  templateUrl: './sound-command-editor.component.html',
  styleUrls: ['./sound-command-editor.component.scss']
})
export class SoundCommandEditorComponent implements OnInit {

  command$: Observable<SoundCommand | null> = of(null);
  commandName: string = NEW_COMMAND_NAME;

  form: UntypedFormGroup;

  commandNameDuplicate$: Observable<boolean>;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly command: SoundCommandsService,
    private readonly fb: UntypedFormBuilder,
    private readonly pageTitle: PageTitleService,
    private readonly dialog: GenericDialogService,
  ) {
    this.form = fb.group({
      channelId: ['', Validators.required],
      commandName: ['', [
        Validators.required,
        Validators.pattern('[a-z]{1,}'),
      ]],
      limitedToMod: [false],
      limitedToUsers: fb.array([]),
      hideFromCommandList: [false],
      cooldown: [0, [Validators.required, Validators.pattern(/\d{1,6}/)]],
      response: ['', [
        Validators.maxLength(400),
      ]],
      soundFiles: fb.array([]),
    });

    this.commandNameDuplicate$ = this.form.get('commandName')!.valueChanges.pipe(
      debounceTime(500),
      filter((commandName: string) => !!commandName && this.inCreationMode),
      map((commandName: string) => commandName.toLowerCase()),
      mergeMap((commandName) => {
        const channel = this.form.get('channelId')!.value;
        return this.command.getCommand(channel, commandName).pipe(
          catchError((e) => of(null)),
        );
      }),
      map((c) => !!c),
      tap((exists) => {
        if (exists) {
          this.form.get('commandName')!.setErrors({
            commandExists: true
          });
        }
      })
    );
  }

  get channelId(): string {
    return this.form.get('channelId')!.value;
  }

  get inCreationMode(): boolean {
    return this.commandName === NEW_COMMAND_NAME;
  }

  get limitUserControls(): UntypedFormArray {
    return this.form.get('limitedToUsers') as UntypedFormArray;
  }

  get soundFilesControls(): UntypedFormArray {
    return this.form.get('soundFiles') as UntypedFormArray;
  }

  get responseLength(): number {
    return (this.form.get('response')?.value as string | undefined)?.length || 0;
  }

  get limitedToModsChecked(): boolean {
    return this.form.get('limitedToMod')?.value || false;
  }

  get whitelistLength(): number {
    return this.limitUserControls.length;
  }

  ngOnInit(): void {
    this.pageTitle.setPageTitle('Sound Commands', 'volume_up');
    const {
      messageInterface,
      channel,
      command
    } = this.activatedRoute.snapshot.params;

    this.commandName = command;
    let command$: Observable<SoundCommand>;
    if (command === NEW_COMMAND_NAME) {
      const newCommand: SoundCommand = {
        channelId: `${messageInterface}/${channel}`,
        commandName: '',
        response: '',
        limitedToMod: false,
        limitedToUsers: [],
        hideFromCommandList: false,
        cooldown: 0,
        payloadId: undefined,
        soundFiles: [],
      };
      command$ = of(newCommand);
    } else {
      command$ = this.command.getCommand(`${messageInterface}/${channel}`, command);
    }
    this.command$ = command$.pipe(
      tap((c) => this.updateForm(c)),
    );
  }

  showLegacyInfo() {
    this.dialog.notify(
      'Heads up! Changes ahead!',
      'With the last update, sound commands have gained the ability to contain more than 1 sound clip, allowing you to play a different clip every time.\n\n' +
      'This however comes with a change: You will now have to upload your sound files through the "Files" section to your channel, before you can use them. ' +
      'The upload function under "Sound Commands" has been removed. ' +
      'Once you press "Save", the previously linked sound file will be deleted.\n\n' +
      'In a future update, all "Legacy" sound files will be deleted, so be sure to upload them to "Files" beforehand.',
      'info'
    );
  }

  private updateForm(command: SoundCommand): void {
    this.pageTitle.setPageTitle(`Sound Command "${command.commandName}"`, 'volume_up');
    this.form.patchValue(command);
    command.limitedToUsers.forEach(u => this.addUser(u));
    command.soundFiles?.forEach(f => this.addSoundFile(f));
  }

  addUser(value: string = ''): void {
    const userForm = this.fb.control(value, [Validators.required]);
    this.limitUserControls.push(userForm);
  }

  removeUser(i: number): void {
    this.limitUserControls.removeAt(i);
  }

  addSoundFile(value: string = ''): void {
    const control = this.fb.control(value, [Validators.required]);
    this.soundFilesControls.push(control);
  }

  removeSoundFile(i: number): void {
    this.soundFilesControls.removeAt(i);
  }

  saveCommand(command: SoundCommand): void {
    console.log(this.form);
    if (!this.form.valid) {
      alert(`Cannot save. Please check your entries and try again.`);
      return;
    }

    const newCommand: SoundCommand = {
      ...command,
      ...this.form.value,
    };

    // Force command name to be lower case
    newCommand.commandName = newCommand.commandName.toLowerCase();
    // Force user names to be lower case
    newCommand.limitedToUsers = newCommand.limitedToUsers
      .filter(u => !!u)
      .map(u => u.toLowerCase());

    if (this.inCreationMode) {
      // Create Command
      this.command.createCommand(newCommand).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not create command due to an error: ${e.message}`);
        }
      });
    } else {
      // Update Command
      this.command.updateCommand(newCommand).subscribe({
        next: () => {
          this.router.navigate(['../'], {
            relativeTo: this.activatedRoute
          });
        },
        error: (e) => {
          alert(`Could not update command due to an error: ${e.message}`);
        }
      });
    }
  }

}
