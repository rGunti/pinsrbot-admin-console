import { ControlValueAccessor, UntypedFormBuilder, UntypedFormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, forwardRef, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { filter } from 'rxjs';
import { FileSelectDialogComponent } from '../file-select-dialog/file-select-dialog.component';

@Component({
  selector: 'app-file-select-input',
  templateUrl: './file-select-input.component.html',
  styleUrls: ['./file-select-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileSelectInputComponent),
      multi: true,
    }
  ]
})
export class FileSelectInputComponent implements ControlValueAccessor {

  @Input() label: string = 'File';
  @Input() channelId: string = '';
  @Input() mimeFilter?: string[];

  form: UntypedFormGroup;

  private onChange: Function = () => {};
  private onTouched: Function = () => {};

  constructor(
    private readonly matDialog: MatDialog,
    fb: UntypedFormBuilder,
  ) {
    this.form = fb.group({
      file: ['']
    });
  }

  getValue() {
    return this.form.get('file')!.value;
  }

  writeValue(obj: any): void {
    console.log('Setting value', obj);
    this.form.get('file')!.patchValue(obj);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  ngOnInit(): void {
  }

  openDialog(): void {
    FileSelectDialogComponent.open(this.matDialog, this.channelId, this.mimeFilter)
      .pipe(
        filter((f) => !!f)
      )
      .subscribe({
        next: (f) => {
          console.log('Setting file', f);
          this.form.get('file')!.setValue(f!.id);
          this.onTouched();
          this.onChange(this.getValue());
        }
      })
  }

}
