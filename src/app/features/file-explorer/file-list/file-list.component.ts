import { Observable, of, tap } from 'rxjs';
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FileHeader, FilesService, Quota } from '../files.service';
import { UploadDialogComponent } from '../upload-dialog/upload-dialog.component';
import { GenericDialogService } from 'src/app/common-controls/generic-dialog/generic-dialog.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
})
export class FileListComponent implements OnInit {

  private initialized: boolean = false;
  private _channelId: string = '';
  private quota?: Quota;

  files$: Observable<FileHeader[]> = of([]);
  quota$: Observable<Quota | null> = of(null);

  constructor(
    private readonly dialog: MatDialog,
    private readonly genericDialog: GenericDialogService,
    private readonly filesService: FilesService,
  ) { }

  @Input() get channelId(): string {
    return this._channelId;
  }
  set channelId(channelId: string) {
    this._channelId = channelId;
    if (this.initialized) {
      this.reload();
    }
  }

  reload(): void {
    this.files$ = this.filesService.listFiles(this.channelId);
    this.quota$ = this.filesService.getQuota(this.channelId).pipe(
      tap((q) => {
        this.quota = q;
      })
    );
  }

  ngOnInit(): void {
    this.reload();
  }

  showQuotaWarning(quota: Quota): boolean {
    return quota.storageUsed / quota.maxStorageQuota >= 0.9
      || (quota.fileCount / quota.maxFileNumber >= 0.9);
  }

  getFileIcon(file: FileHeader): string {
    return this.filesService.getFileIcon(file);
  }

  downloadFile(file: FileHeader): void {
    this.genericDialog.snack('Download will begin shortly ...');
    this.filesService.downloadFile(this.channelId, file.fileName).subscribe({
      next: (f) => {
        let url = window.URL.createObjectURL(f);
        let a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = url;
        a.download = file.fileName;
        a.click();
        window.URL.revokeObjectURL(url);
        a.remove();
      },
      error: (e) => {
        this.genericDialog.snack('Failed to download file');
      }
    });
  }

  async delete(file: FileHeader): Promise<void> {
    if (!await this.genericDialog.ask('Delete file?', `Do you want to delete ${file.fileName}?\nThis operation is irreversible!`, undefined, undefined, 'warn')) {
      return;
    }

    this.filesService.deleteFile(this.channelId, file.fileName).subscribe({
      next: () => {
        this.genericDialog.snack('File deleted');
        this.reload();
      },
      error: (e) => {
        this.genericDialog.snack(`Failed to delete file (Error Code ${e.status})`);
      }
    });
  }

  upload(): void {
    if (this.quota && (this.quota.fileCount >= this.quota.maxFileNumber || this.quota.storageUsed >= this.quota.maxStorageQuota)) {
      this.genericDialog.notify(
        'Limit reached',
        'You cannot upload any more files because you have either reached the maximum number of files you can store or you have run out of storage space.\n\n' +
        'Delete some files to upload more.',
        'warning'
      );
      return;
    }
    UploadDialogComponent.open(this.dialog, this.channelId).subscribe({
      next: () => {
        this.reload();
      }
    });
  }

}
